// Miscellanous things go here
$(document).ready(function () {
    // check if data is sent from backend
    if ($.isArray(creds) && creds !== undefined && creds.length != 0) {
        $('body').append('<div id="dynamic_data"></div>');
        $('#dynamic_data').css({
            "display": "none",
        });
        for (i = 0; i < creds.length; i++) {
            $('#dynamic_data').append('<p>' + creds[i] + '</p>');
        }
    }
});

/*
* Interesting
* For template literals, eg `${nu}`, use `` not '' or ""
*/

// use to add zeros(0's) to single digit number strings
function appendLeadingZeroes(n) {
    if (n <= 9) {
        return "0" + n;
    }
    return n
};

// format issue date
currentDate = function () {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    let current_datetime = new Date();
    return '<b>' + appendLeadingZeroes(current_datetime.getDate()) + " " + months[current_datetime.getMonth()] + " " + current_datetime.getFullYear() + '</b>';
};

// format issue date
currentTime = function () {
    let current_datetime = new Date();
    let hours = current_datetime.getHours();
    let ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    return '<b>' + appendLeadingZeroes(hours) + ":" + appendLeadingZeroes(current_datetime.getMinutes()) + ":" + appendLeadingZeroes(current_datetime.getSeconds()) + " " + ampm + '</b>';
};

$('#issue_date').html("Date of Issue:   " + currentDate());
$('#issue_time').html("Time of Issue:   " + currentTime());

// we support special element handlers. Register them with jQuery-style 
// ID selector for either ID or node. (ID: "#iAmID", Node: "div", "span" etc.)
let specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};

// set dimensions for PDF page
let options = { orientation: 'p', unit: 'mm', format: 'a4', };
// create instance of JSPDF with above dimensions
let pdf = new jsPDF(options);
let base64Icon = null;
let base64Approval = null;
let base64QR = null;
let table1 = null;
let table2 = null;
let table3 = null;
let table4 = null;
let officer_section = null;

// for speed and efficiency, preload all images and convert to base64 string
// 1. Load icon
imgToBase64('assets/images/ic_barcode.jpg', function (base64) { base64Icon = base64; });
// 2. Approval seal
imgToBase64('assets/images/approval_seal.jpg', function (base64) { base64Approval = base64; });
// set up canvas for html screenshots
var w = 1000;
var h = 1000;
var canvas = document.createElement('canvas');
canvas.width = w * 2;
canvas.height = h * 2;
canvas.style.width = w + 'px';
canvas.style.height = h + 'px';
var context = canvas.getContext('2d');
context.scale(2, 2);

// spacings and size for html
margins = {
    top: 70,
    bottom: 40,
    left: 30,
    width: 550,
    height: 550,
};

// This function creates PDF and sets it on screen
generatePDF = function () {
    // 3. QR code
    let data = $("#data").text();
    var url = `https://jiji-manage.herokuapp.com/https://qrickit.com/api/qr.php?d=${data}&t=j`;
    imgToBase64(url, function (base64) { base64QR = base64; });
    // 4. Screenshot table 1
    html2canvas(document.getElementById("table1"), {
        dpi: 1500,
        scale: 2,
        canvas: canvas,
    }).then(function (canvas) {
        // Export the canvas to its data URI representation
        table1 = canvas.toDataURL("image/png");
    });
    // 5. Screenshot table 2
    html2canvas(document.getElementById("table2"), {
        dpi: 1500,
        scale: 2,
        canvas: canvas,
    }).then(function (canvas) {
        // Export the canvas to its data URI representation
        table2 = canvas.toDataURL("image/png");
    });
    // 6. Screenshot table 3
    html2canvas(document.getElementById("table3"), {
        dpi: 1500,
        scale: 2,
        canvas: canvas,
    }).then(function (canvas) {
        // Export the canvas to its data URI representation
        table3 = canvas.toDataURL("image/png");
    });
    // 6. Screenshot table 42
    html2canvas(document.getElementById("table4"), {
        dpi: 1500,
        scale: 2,
        canvas: canvas,
    }).then(function (canvas) {
        // Export the canvas to its data URI representation
        table4 = canvas.toDataURL("image/png");
    });
    // 7. Screenshot signature section
    html2canvas(document.getElementById("officer_section"), {
        dpi: 1500,
        scale: 2,
        canvas: canvas,
    }).then(function (canvas) {
        // Export the canvas to its data URI representation
        officer_section = canvas.toDataURL("image/png");
    });
    pdf.setFontSize(10);
    // if any of the images from html2canvas are not ready, disable pdf generation
    if (table1 === null || table2 === null || table3 === null || table4 === null ||
        base64Approval === null || base64QR === null || officer_section === null) {
        showSnackbar("PDF not ready. Please retry");
    } else {
        // add app icon
        pdf.addImage(base64Icon, 'JPEG', 5, 5, 40, 40);

        // add HTML
        htmlIntoPDF(document.getElementById('title'), 55, 5);
        htmlIntoPDF(document.getElementById('provisional'), 55, 13);
        htmlIntoPDF(document.getElementById('permit_duration'), 125, 35);

        pdf.addImage(table1, 'PNG', 5, 59, 200, 50);

        pdf.addImage(table2, 'PNG', 5, 113, 200, 45);

        pdf.addImage(table3, 'PNG', 5, 162, 200, 13);

        pdf.addImage(table4, 'PNG', 5, 180, 200, 40);

        htmlIntoPDF(document.getElementById('issue_date'), 5, 218);
        htmlIntoPDF(document.getElementById('issue_time'), 155, 218);

        // add approval seal
        pdf.addImage(base64Approval, 'JPEG', 5, 248, 40, 40);

        // add QR Code
        pdf.addImage(base64QR, 'JPEG', 170, 248, 40, 40);

        // ad signature section
        pdf.addImage(officer_section, 'PNG', 55, 244, 100, 50);

        htmlIntoPDF(document.getElementById('notice'), 5, 230);

        // get current time and format it
        let now = new Date();
        let current_datetime = appendLeadingZeroes(now.getFullYear()) + "_" + appendLeadingZeroes(now.getMonth()) + "_" + appendLeadingZeroes(now.getDate()) + "_" + appendLeadingZeroes(now.getHours()) + "_" + appendLeadingZeroes(now.getMinutes()) + "_" + appendLeadingZeroes(now.getSeconds());

        // if device is desktop and above
        if ($(window).width() > 991) {
            // create iframe to render generate PDF on screen
            let iframe = document.createElement('iframe');
            iframe.setAttribute('id', 'pdfIframe');
            iframe.setAttribute('style', 'position:absolute;right:0; top:0; bottom:0; height:100%; width:650px; padding:20px;');
            document.body.appendChild(iframe);
            // this is where the pdf is added to iframe
            const file_header = 'headers=filename%3D';
            const fileName = $('p#business_name').text() + current_datetime + ".pdf";
            let res = pdf.output('datauristring').replace("filename=generated.pdf", file_header + encodeURIComponent(fileName.replace(" ", "_")));
            iframe.src = res;
            // remove margin around generate button
            $('#generateBtn').css({ 'margin-bottom': "auto" });
            // reveal download button
            $('#downloadBtn').css({ 'display': "block" });
            // reveal hide button
            $('#hideBtn').css({ 'display': 'block' });
        } else {
            const fileName = $('p#business_name').text() + current_datetime + ".pdf";
            // save pdf
            pdf.save(fileName.replace(" ", "_"));
        }
    }
};

// This function downloads the created PDF and then removes the PDF from screen
downloadPDF = function () {
    // get current time and format it
    let now = new Date();
    let current_datetime = appendLeadingZeroes(now.getFullYear()) + "_" + appendLeadingZeroes(now.getMonth()) + "_" + appendLeadingZeroes(now.getDate()) + "_" + appendLeadingZeroes(now.getHours()) + "_" + appendLeadingZeroes(now.getMinutes()) + "_" + appendLeadingZeroes(now.getSeconds());
    // set filename
    let fileName = $('p#business_name').text() + current_datetime + ".pdf";
    // save pdf
    pdf.save(fileName.replace(" ", "_"));
    // remove pdf
    hidePDF();
};

// This function removes PDF from screen
hidePDF = function () {
    // Check if iFrame exists
    if ($("#pdfIframe").length) {
        // return margin around generate button
        $('#generateBtn').css({ 'margin-bottom': "10px" });
        // remove PDF from screen
        $("#pdfIframe").remove();
        // hide download button
        $('#downloadBtn').css({ 'display': 'none' });
        // hide hide button
        $('#hideBtn').css({ 'display': 'none' });
    }
};

// This function removes the element whose ID is provided from screen
removeDiv = function (elem_id) {
    // check if element exists
    if ($(elem_id).length) {
        // inform user of success clearing
        showSnackbar("Successful clearing");
        // remove element
        $(elem_id).remove();
        // remove iframe if exists as well
        if ($("#pdfIframe").length) { $("#pdfIframe").remove(); }
    }
    // restore original screen.
    // we are calling dart code here
    restore();
};

// You could either use a function similar to this or pre convert an image with for example http://dopiaza.org/tools/datauri
// http://stackoverflow.com/questions/6150289/how-to-convert-image-into-base64-string-using-javascript
function imgToBase64(url, callback, imgVariable) {

    if (!window.FileReader) {
        callback(null);
        return;
    }
    let xhr = new XMLHttpRequest();
    xhr.responseType = 'blob';
    xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
            imgVariable = reader.result.replace('text/xml', 'image/jpeg');
            callback(imgVariable);
        };
        reader.readAsDataURL(xhr.response);
    };
    xhr.open('GET', url);
    xhr.send();
};

// Here, we shall be calling HTML directly into PDF
// Takes an HTML element eg a div as argument
function htmlIntoPDF(element, left, top) {
    // add HTML to PDF here
    pdf.fromHTML(element,
        left, // x-coordinate value from left edge
        top, // y-coordinate value from top edge
        // any extra settings eg size
        {
            elementHandlers: specialElementHandlers,
            width: margins.width, // max width of content on PDF
            // height: height, // max height of content on PDF
        },
        // callback function used to perform extra functionality on the pages generated
        function (dispose) { },
        // margins
        margins,
    );
};

// Here, we shall set up toast.
// This will enable showing data to user
function showSnackbar(message) {
    var x = document.getElementById("snackbar");
    x.innerHTML = message;
    x.className = "show";
    setTimeout(function () { x.className = x.className.replace("show", ""); }, 3000);
};
