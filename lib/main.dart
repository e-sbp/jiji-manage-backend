import 'package:flutter/material.dart';
import 'login.dart';
import 'themes.dart';
import 'extras.dart';
import 'landing.dart';
import 'dart:html';

void main() => runApp(JijiManage());

class JijiManage extends StatelessWidget {
  Widget homePage() {
    var div = querySelector("#dynamic_data");
    var creds = [];
    if (div != null) {
      div.children.forEach((childElem) => creds.add(childElem.text));
      print('Hi: $creds');
      document.body.children.remove(div);
      return LandingPage("${creds[0]}", "${creds[1]}", "${creds[2]}",
          "${creds[2]}", "${creds[3]}",
          isPhone: true);
    }
    return LoginScreen();
  }

  @override
  Widget build(BuildContext context) {
    htmlRender();
    print(stringToColour('${parserReceivedTime("20160625131011")}'));
    return MaterialApp(
      title: 'Mombasa County e-SBP',
      debugShowCheckedModeBanner: false,
      theme: myAppTheme,
      home: homePage(),
      color: Colors.blue,
    );
  }
}
