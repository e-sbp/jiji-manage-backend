import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'accent_override.dart';
import 'colors.dart';
import 'extras.dart';
import 'urls.dart';

class RegisterDeviceScreen extends StatefulWidget {
  final String token;
  final Map<String, String> details;

  RegisterDeviceScreen(this.token, {this.details});

  _RegisterDeviceScreenState createState() => _RegisterDeviceScreenState();
}

class _RegisterDeviceScreenState extends State<RegisterDeviceScreen> {
  String deviceID = '', deviceName = 'Android', userID;
  List<String> devices = ['Android', 'iOS'];
  List<dynamic> users = [];
  int _radioValue = 0;
  Map<String, String> details = {};

  final idController = TextEditingController();

  @override
  void initState() {
    super.initState();

    details = widget.details ?? {};
    deviceID = details['device_id'] ?? '';
    deviceName = details['device_name'] ?? 'Android';
    userID = details['user_id'];
    _radioValue = devices.indexOf(deviceName);

    idController.text = deviceID;

    loadUsers();
  }

  void _deviceID(String str) {
    if (!mounted) return;
    setState(() => deviceID = str.toLowerCase().trim());
  }

  void _setDevice(int value) {
    if (!mounted) return;
    setState(() {
      _radioValue = value;
      switch (_radioValue) {
        case 0:
          deviceName = devices[0];
          break;
        case 1:
          deviceName = devices[1];
          break;
      }
    });
  }

  void _role(String str) {
    if (!mounted) return;
    setState(() => userID = str.toLowerCase().trim());
  }

  void loadUsers() async {
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => users = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/active/users/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() => users = json.decode(response.body));
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => users = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => users = []);
      });
    }
  }

  Future<dynamic> handleRegister(Map<String, String> value) async {
    if (isStringEmpty(widget.token) ||
        isStringEmpty(apiKey) ||
        (details.isNotEmpty & isStringEmpty(details["device_id"]))) {
      return "Something missing. Please try again later";
    }
    var response;
    if (details.isEmpty) {
      response = await http.post(
        '$proxyUrl$baseUrl/500/device/registration/',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    } else {
      response = await http.put(
        '$proxyUrl$baseUrl/30/device/edit/${details["device_id"]}/',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    }
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _registerDevice() {
    if (!mounted) return;
    setState(() {
      if (isStringEmpty(deviceID)) {
        showToast("Please enter the device ID (serial number)");
      } else if (isStringEmpty(deviceName)) {
        showToast("Please select the device name");
      } else if (isStringEmpty(userID)) {
        showToast("Please select the user");
      } else {
        Map<String, String> values = {
          "device_id": deviceID,
          "device_name": deviceName,
          "user_id": userID,
        };
        handleRegister(values).then((res) {
          if (res is String) {
            showToast(res);
            return;
          }
          final errorCode = res['errorCode'];
          final desc = res['desc'];
          if (desc == null || errorCode == null) {
            showToast("Error while registering in");
            return;
          }
          showToast("$desc");
          print("$errorCode");
          if (int.parse(errorCode) == 200) {
            if (details.isEmpty) {
              landingState.setState(() => landingState.screen = "home");
            } else {
              Navigator.of(context).pop();
            }
          }
        }).catchError((err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: landingState.controller,
      child: users.isEmpty
          ? Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 30.0),
                    Text(
                      "Waiting for users...",
                      style: TextStyle(color: Colors.black, fontSize: 16.0),
                    ),
                  ],
                ),
              ),
              height: MediaQuery.of(context).size.height / 2,
            )
          : Center(
              child: Container(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 8.0),
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    // Given a child, this widget forces child to have a specific width and/or height
                    // else it will adjust itself to given dimension
                    SizedBox(height: 20.0),
                    // display children in vertical array
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'images/ic_barcode.jpg',
                          scale: 2.0,
                        ),
                        SizedBox(height: 10.0),
                        Material(
                          child: Text(
                            'E-SBP',
                            style: TextStyle(
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    // Device ID
                    Container(
                      child: Center(
                        child: AccentOverride(
                          child: TextField(
                            enabled: details.isEmpty ? true : false,
                            decoration: InputDecoration(
                              labelText: 'Device ID',
                              helperText: "i.e Device Serial Number",
                              helperStyle: TextStyle(
                                color: Colors.black,
                                fontSize: 12.0,
                              ),
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                            ),
                            onChanged: _deviceID,
                            controller: idController,
                          ),
                        ),
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    Container(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Device Name: ',
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(height: 30.0),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                children: <Widget>[
                                  Radio(
                                    value: 0,
                                    groupValue: _radioValue,
                                    onChanged: details.isEmpty
                                        ? (value) => _setDevice(value)
                                        : (value) => showToast(
                                              "Cannot change device name",
                                            ),
                                  ),
                                  Row(
                                    children: [
                                      Icon(Icons.phone_android),
                                      Text(
                                        devices[0],
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Radio(
                                    value: 1,
                                    groupValue: _radioValue,
                                    onChanged: details.isEmpty
                                        ? (value) => _setDevice(value)
                                        : (value) => showToast(
                                            "Cannot change device name"),
                                  ),
                                  Row(
                                    children: [
                                      Icon(Icons.phone_iphone),
                                      Text(
                                        'iOS',
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          color: Colors.black,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    Container(
                      child: Row(
                        children: [
                          Icon(Icons.person),
                          Flexible(
                            child: DropdownButton(
                              isExpanded: true,
                              hint: Text("Please select a user"),
                              items: users.map((value) {
                                return value is Map
                                    ? DropdownMenuItem<String>(
                                        value: value['user_id'],
                                        child: Text(
                                          '${value['first_name']} ${value['last_name']}',
                                        ),
                                      )
                                    : DropdownMenuItem<String>(
                                        value: value,
                                        child: Text('$value'),
                                      );
                              }).toList(),
                              onChanged: (value) => _role(value),
                              value: userID,
                            ),
                          ),
                        ],
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    // Places the buttons horizontally according to the padding
                    Container(
                      child: Row(
                        children: <Widget>[
                          RaisedButton(
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                color: myBackgroundColor,
                                fontSize: 15.0,
                              ),
                            ),
                            onPressed: () => _registerDevice(),
                            shape: StadiumBorder(),
                            color: Colors.green,
                            elevation: 8.0,
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
