/// Define app bar to avoid typing it everywhere

import 'package:flutter/material.dart';

import 'landing.dart' show landingState;

class AppTitle extends AppBar {
  final bool loggedIn;
  final Function logout;
  final Function refresh;

  AppTitle(this.logout, {this.loggedIn, this.refresh})
      : super(
          title: GestureDetector(
            child: Text(
              "Mombasa County e-SBP",
              style: TextStyle(color: Colors.white),
            ),
            onTap: landingState.screen == "home" ? () {} : logout ?? () {},
          ),
          backgroundColor: Colors.blue,
          centerTitle: true,
          actions: <Widget>[
            // App icon
            Container(
              margin: EdgeInsets.only(right: 10.0),
              width: 30.0,
              height: 30.0,
              decoration: BoxDecoration(
                shape: BoxShape.rectangle,
                image: DecorationImage(
                  fit: BoxFit.scaleDown,
                  image: AssetImage("images/ic_barcode.jpg"),
                ),
              ),
            ),
            // Reload screen
            refresh != null
                ? Container(
                    child: IconButton(
                      icon: Icon(
                        Icons.refresh,
                        color: Colors.white,
                        size: 22.0,
                        semanticLabel: "Refresh",
                      ),
                      tooltip: "Refresh",
                      onPressed: refresh,
                    ),
                    margin: EdgeInsets.only(right: 10.0),
                  )
                : Container(width: 0, height: 0),
            // Logout
            (loggedIn == null || loggedIn) && logout != null
                ? Container(
                    child: IconButton(
                      icon: Icon(
                        Icons.exit_to_app,
                        color: Colors.white,
                        size: 22.0,
                        semanticLabel: "Logout",
                      ),
                      tooltip: "Logout",
                      onPressed: logout,
                    ),
                    padding: EdgeInsets.only(right: 10.0),
                  )
                : Container(width: 0, height: 0),
          ],
        );
}
