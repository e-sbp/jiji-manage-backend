import 'package:flutter/material.dart';

import 'draggable_scrollbar.dart';
import 'register_user.dart';
import 'register_device.dart';
import 'register_market.dart';
import 'register_sbp.dart';
import 'view_user.dart';
import 'view_device.dart';
import 'view_market.dart';
import 'view_sbp.dart';
import 'edit_user.dart';
import 'edit_device.dart';
import 'edit_market.dart';
import 'edit_sbp.dart';
import 'home.dart';
import 'title.dart';
import 'extras.dart';

_LandingPageState landingState = _LandingPageState();

class LandingPage extends StatefulWidget {
  final String token;
  final String username;
  final String user_id;
  final String role;
  final String fullname;
  final bool isPhone;

  LandingPage(this.token, this.username, this.user_id, this.role, this.fullname,
      {this.isPhone = false});

  _LandingPageState createState() {
    if (landingState == null) {
      landingState = _LandingPageState();
    }
    return landingState;
  }
}

class _LandingPageState extends State<LandingPage> {
  String token = '', username = '', role = '', fullname = '';
  Map<String, Widget> screens;
  String screen = 'home';

  bool editClosed = true, registerClosed = true, viewClosed = true;
  Function refresh;
  num opacity = 1.0;

  final ScrollController controller = ScrollController();

  @override
  void initState() {
    super.initState();
    token = widget.token ?? '';
    username = widget.username ?? '';
    role = widget.role ?? '';
    fullname = widget.fullname ?? '';
    screens = {
      "home": HomeScreen(token, username),
      "register_user": RegisterUserScreen(token),
      "register_phone": RegisterDeviceScreen(token),
      "register_market": RegisterMarketScreen(token),
      "register_sbp": RegisterSBPScreen(token),
      "view_user": ViewUserScreen(token),
      "view_device": ViewDeviceScreen(),
      "view_market": ViewMarketScreen(token),
      "view_sbp": ViewSBPScreen(token),
      "edit_user": EditUserScreen(token, username),
      "edit_device": EditDeviceScreen(token, username),
      "edit_market": EditMarketScreen(token),
      "edit_sbp": EditSBPScreen(token),
    };
  }

  @override
  void dispose() {
    super.dispose();
    controller.dispose();
    refresh = null;
    landingState = null;
  }

  Widget menuItems(String title, IconData leadingIcon, String action,
      {bool isMargin = true, bool close = false}) {
    return Container(
      margin: EdgeInsets.only(left: isMargin ? 20.0 : 0),
      child: ListTile(
        title: Text(
          title,
          style: TextStyle(color: Colors.white, fontSize: 18.0),
        ),
        leading: Icon(leadingIcon, color: Colors.white),
        onTap: () {
          Navigator.of(context).pop();
          setState(() {
            screen = action;
            if (action == "home") landingState.refresh = null;
            if (!close) {
              if (action.contains("register")) {
                registerClosed = !registerClosed;
              } else if (action.contains("view")) {
                viewClosed = !viewClosed;
              } else if (action.contains("edit")) {
                editClosed = !editClosed;
              }
            }
          });
        },
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: opacity == 1.0
          ? AppTitle(
              widget.isPhone
                  ? null
                  : screen == "home"
                      ? () => logout(context, token, username)
                      : () => landingState.setState(() {
                            landingState.screen = "home";
                            landingState.refresh = null;
                          }),
              refresh: refresh,
            )
          : AppBar(
              leading: Container(),
              title: Text(
                "Mombasa County e-SBP",
                style: TextStyle(color: Colors.white),
              ),
              backgroundColor: Colors.blue,
              centerTitle: true,
              actions: <Widget>[
                Container(
                  margin: EdgeInsets.only(right: 10.0),
                  width: 30.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    image: DecorationImage(
                      fit: BoxFit.scaleDown,
                      image: AssetImage("images/ic_barcode.jpg"),
                    ),
                  ),
                ),
                Container(
                  child: IconButton(
                    icon: Icon(
                      Icons.exit_to_app,
                      color: Colors.white,
                      size: 22.0,
                      semanticLabel: "Logout",
                    ),
                    tooltip: "Logout",
                    onPressed: () => landingState.setState(
                      () {
                        landingState.screen = "home";
                        landingState.refresh = null;
                      },
                    ),
                  ),
                  padding: EdgeInsets.only(right: 10.0),
                )
              ],
            ),
      drawer: Opacity(
        child: Drawer(
          child: Container(
            color: Colors.blue,
            child: ListView(
              children: <Widget>[
                drawerTitle(),
                userSection(),
                SizedBox(
                  width: 10.0,
                  height: 2,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: Colors.white),
                  ),
                ),
                menuItems("Home Page", Icons.home, 'home', isMargin: false),
                registerMenu(),
                editMenu(),
                viewMenu(),
                menuItems("Update profile", Icons.person_pin, 'home',
                    isMargin: false),
                menuItems("Settings", Icons.settings, 'home', isMargin: false),
                SizedBox(
                  width: 10.0,
                  height: 2,
                  child: DecoratedBox(
                    decoration: BoxDecoration(color: Colors.white),
                  ),
                ),
                menuItems("Close", Icons.close, screen,
                    isMargin: false, close: true),
              ],
            ),
          ),
        ),
        opacity: opacity,
      ),
      // body: DraggableScrollbar(
      //   child: screens[screen],
      //   heightScrollThumb: 40.0,
      //   controller: controller,
      // ),
      body: screens[screen],
      bottomNavigationBar: Container(
        child: Row(
          children: <Widget>[
            Expanded(
              child: Text('\u00a9 Mombasa County e-SBP, 2019'),
            ),
            Expanded(
              child: Text('Powered by Emix Technologies'),
            ),
          ],
        ),
        height: 50.0,
      ),
    );
  }

  /// Contain app name and/or icon
  Widget drawerTitle() {
    return Padding(
      child: Text(
        "Mombasa County e-SBP",
        style: TextStyle(
          color: Colors.white,
          fontSize: 24.0,
        ),
        textAlign: TextAlign.center,
      ),
      padding: EdgeInsets.only(top: 12.0),
    );
  }

  Widget userSection() {
    return Container(
      margin: EdgeInsets.only(top: 1.0, left: 1.0),
      width: MediaQuery.of(context).size.width,
      height: 100.0,
      child: Row(
        children: [
          Padding(
            padding: EdgeInsets.only(right: 10.0),
          ),
          Container(
            width: 50.0,
            height: 50.0,
            margin: EdgeInsets.only(left: 2.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.white,
                width: 2.0,
              ),
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: NetworkImage(
                  userProfilePicture(widget.user_id, '128'),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(right: 50.0),
          ),
          Column(
            children: <Widget>[
              Text(
                fullname,
                style: TextStyle(color: Colors.white, fontSize: 18.0),
              ),
              Padding(
                padding: EdgeInsets.only(bottom: 5.0),
              ),
              Text(
                role,
                style: TextStyle(color: Colors.white, fontSize: 16.0),
              ),
            ],
            mainAxisAlignment: MainAxisAlignment.center,
          )
        ],
      ),
    );
  }

  Widget registerMenu() {
    return ExpansionTile(
      title: Text(
        "Register",
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      leading: Icon(
        Icons.add,
        color: Colors.white,
      ),
      trailing: Icon(
        registerClosed ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,
        color: Colors.white,
      ),
      onExpansionChanged: (state) {
        setState(() => registerClosed = !registerClosed);
      },
      children: <Widget>[
        menuItems("User", Icons.person, 'register_user'),
        menuItems("Device", Icons.smartphone, 'register_phone'),
        menuItems("Market", Icons.business_center, 'register_market'),
        menuItems("SBP", Icons.business, 'register_sbp'),
      ],
    );
  }

  Widget editMenu() {
    return ExpansionTile(
      title: Text(
        "Make Changes",
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      leading: Icon(
        Icons.edit,
        color: Colors.white,
      ),
      trailing: Icon(
        editClosed ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,
        color: Colors.white,
      ),
      onExpansionChanged: (state) {
        setState(() => editClosed = !editClosed);
      },
      children: <Widget>[
        menuItems("User", Icons.person, 'edit_user'),
        menuItems("Device", Icons.smartphone, 'edit_device'),
        menuItems("Market", Icons.business_center, 'edit_market'),
        menuItems("SBP", Icons.business, 'edit_sbp'),
      ],
    );
  }

  Widget viewMenu() {
    return ExpansionTile(
      title: Text(
        "View",
        style: TextStyle(color: Colors.white, fontSize: 18.0),
      ),
      leading: Icon(
        Icons.add,
        color: Colors.white,
      ),
      trailing: Icon(
        viewClosed ? Icons.keyboard_arrow_down : Icons.keyboard_arrow_up,
        color: Colors.white,
      ),
      onExpansionChanged: (state) {
        setState(() => viewClosed = !viewClosed);
      },
      children: <Widget>[
        menuItems("User", Icons.person, 'view_user'),
        menuItems("Device", Icons.smartphone, 'view_device'),
        menuItems("Market", Icons.business_center, 'view_market'),
        menuItems("SBP", Icons.business, 'view_sbp'),
      ],
    );
  }
}
