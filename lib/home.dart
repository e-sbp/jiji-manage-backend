import 'dart:async';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:html';

import 'static_maps_provider.dart';
import 'landing.dart' show landingState;
import 'extras.dart';
import 'urls.dart';

class HomeScreen extends StatefulWidget {
  final String token;
  final String username;

  HomeScreen(this.token, this.username);

  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  String data = '';
  var element;
  final performCardTextColor = Colors.blueGrey;
  ScrollController _controller;
  bool visible = false;
  String mapError = '';
  AnimationController _animationController;

  // If difference is 0, GPS is today's,
  // If difference is 1, GPS is yesterday's
  // If difference is 14, GPS is from 2 weeks ago
  int desiredDiffInDays = 0;
  List<String> durations = ['Today', 'Yesterday', 'This month', 'This cycle'];
  String general = 'Today', clerks = 'Today', tags = 'Today';

  /// Will hold a map containing errorcode as key
  /// and the number of SBPs per code as value
  var scans;
  Timer _timer; // for refreshing screen periodically
  /// Holds the last received GPS coordinates for all devices
  Map<String, List<String>> last_locations = {};
  Map<String, List<Map<String, dynamic>>> all_locations = {};

  // Hold details of users and markets
  var users;
  var markets;

  // Change the current period under review
  void general_period(String str) => setState(() => general = str);
  void clerk_period(String str) => setState(() => clerks = str);
  void tag_period(String str) => setState(() => tags = str);

  // Here, we set period for all segments
  void _period(String str) => setState(() {
        general = str;
        clerks = str;
        tags = str;
      });

  /// Here, we shall detect if user has scrolled 50 pixels from the top
  /// If so, we shall show the FAB to return to top

  _scrollListener() {
    if (_controller.offset >= 50.0) {
      setState(() => visible = true);
    } else {
      setState(() => visible = false);
    }
  }

  /// This function shall animate page back to top when the FAB is pressed

  _moveUp() {
    _controller.animateTo(0,
        curve: Curves.linear, duration: Duration(milliseconds: 500));
  }

  void loadUsers() async {
    setState(() => users = null);
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      setState(() => users = "Something missing. Try logging in again");
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/users/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() => users = json.decode(response.body));
        } else {
          logError("${response.statusCode}", "${response.body}");
          setState(
            () => users = "Something went wrong. Please try again later",
          );
        }
      }).catchError((err) {
        setState(() => users = "Something went wrong. Please try again later");
        print("$err");
      });
    }
  }

  void loadMarkets() async {
    setState(() => markets = null);
    if (isStringEmpty(widget.token)) {
      setState(() => markets = "Something missing. Try logging in again");
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/markets/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() => markets = json.decode(response.body));
        } else {
          logError("${response.statusCode}", "${response.body}");
          setState(
              () => markets = "Something went wrong. Please try again later");
        }
      }).catchError((err) {
        setState(
          () => markets = "Something went wrong. Please try again later",
        );
        print("$err");
      });
    }
  }

  @override
  void initState() {
    super.initState();
    // Control blinking widget
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat();
    // control scrolling and enable scroll to top
    _controller = landingState.controller;
    _controller.addListener(_scrollListener);
    // set up all the sections
    getAllScans();
    buildMarkers();
    loadUsers();
    loadMarkets();
    // repeat after some time
    _timer = Timer.periodic(Duration(minutes: 10), (_) {
      buildMarkers();
      getAllScans();
      loadUsers();
      loadMarkets();
    });
  }

  @override
  void dispose() {
    _animationController.dispose();
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.symmetric(vertical: 18.0, horizontal: 15.0),
          child: ListView(
            children: <Widget>[
              Container(
                height: 175.0,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: GestureDetector(
                        child: totals(durations[0], 2379, 6, true, false),
                        onTap: () => _period(durations[0]),
                      ),
                    ),
                    VerticalDivider(color: Colors.grey, width: 20),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              child:
                                  totals(durations[1], 2378, 0, false, false),
                              onTap: () => _period(durations[1]),
                            ),
                          ),
                        ],
                      ),
                    ),
                    VerticalDivider(color: Colors.grey, width: 20),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              child:
                                  totals(durations[2], 10394, 6, false, true),
                              onTap: () => _period(durations[2]),
                            ),
                          ),
                        ],
                      ),
                    ),
                    VerticalDivider(color: Colors.grey, width: 20),
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: GestureDetector(
                              child:
                                  totals(durations[3], 12379, 6, true, false),
                              onTap: () => _period(durations[3]),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                color: Color(0xFFF5F5F5),
                margin: EdgeInsets.symmetric(vertical: 20.0),
                padding: EdgeInsets.symmetric(vertical: 10.0),
              ),
              segmentHeader("General Information", general_period, general),
              SizedBox(
                width: 10.0,
                height: 3,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(vertical: 20.0),
                child: Row(
                  children: <Widget>[
                    !isStringEmpty(mapError)
                        ? homeErrorWidget(mapError)
                        : Expanded(
                            flex: 3,
                            child: StaticMapsProvider(
                              widget.token,
                              widget.username,
                              last_locations,
                              all_locations: all_locations,
                              zoom: '10',
                            ),
                          ),
                    MediaQuery.of(context).size.width >= 800
                        ? scans == null
                            ? Expanded(flex: 1, child: loadingWidget())
                            : scans is String
                                ? homeErrorWidget
                                : scansPerformance()
                        : SizedBox(width: 0),
                  ],
                ),
                height: 300.0,
              ),
              SizedBox(
                width: 10.0,
                height: 3,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  vertical: MediaQuery.of(context).size.width < 800 ? 0 : 20.0,
                ),
                child: Row(
                  children: <Widget>[
                    MediaQuery.of(context).size.width < 800
                        ? scans == null
                            ? Expanded(flex: 1, child: loadingWidget())
                            : scans is String
                                ? homeErrorWidget(scans)
                                : scansPerformance()
                        : SizedBox(width: 0.0),
                  ],
                ),
                height: MediaQuery.of(context).size.width < 800 ? 300.0 : 0,
              ),
              MediaQuery.of(context).size.width > 800
                  ? SizedBox(height: 0.0)
                  : Container(
                      child: SizedBox(
                        width: 10.0,
                        height: 3,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            color: Color(0xFFEEEEEE),
                          ),
                        ),
                      ),
                      margin: EdgeInsets.symmetric(vertical: 5.0),
                    ),
              segmentHeader("Clerks Performance", clerk_period, clerks),
              SizedBox(
                width: 10.0,
                height: 3,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
              Container(
                child: users == null
                    ? loadingWidget()
                    : users is String
                        ? Row(children: <Widget>[homeErrorWidget(users)])
                        : sectionContainer("users"),
                margin: EdgeInsets.symmetric(vertical: 20.0),
              ),
              SizedBox(
                width: 10.0,
                height: 3,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
              Container(
                child: segmentHeader("Markets Performance", tag_period, tags),
                margin: EdgeInsets.symmetric(vertical: 10.0),
              ),
              SizedBox(
                width: 10.0,
                height: 3,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
              Container(
                child: markets == null
                    ? loadingWidget()
                    : markets is String
                        ? Row(children: <Widget>[homeErrorWidget(markets)])
                        : sectionContainer("markets"),
                margin: EdgeInsets.symmetric(vertical: 20.0),
              ),
              SizedBox(
                width: 10.0,
                height: 3,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
            ],
            controller: _controller,
          ),
        ),
        Positioned(
          bottom: 10.0,
          right: 20.0,
          child: visible
              ? Container(
                  // FAB for scrolling to top
                  child: FloatingActionButton(
                    onPressed: _moveUp,
                    child: Icon(
                      Icons.keyboard_arrow_up,
                      size: 36.0,
                    ),
                    tooltip: "Scroll to top",
                  ),
                  width: 50.0,
                  height: 50.0,
                )
              : Container(),
        ),
      ],
    );
  }

  /// Widget when problem encountered
  Widget homeErrorWidget(String message) {
    return Expanded(
      flex: 1,
      child: Center(
        child: Text(
          message,
          style: TextStyle(
            color: Colors.black,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }

  /// Set up the static map by getting last positions of devices
  buildMarkers() async {
    // get today's date
    DateTime now = DateTime.now();
    DateTime today = DateTime(now.year, now.month, now.day);
    if (isStringEmpty(widget.token)) {
      setState(() {
        mapError = "Something missing. Try logging in again";
        last_locations = {};
      });
    } else {
      Stopwatch stopwatch = Stopwatch()..start();
      await http.get(
        '$proxyUrl$sbpUrl/800/device/gps/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          List coordinates = json.decode(response.body);
          if (coordinates.isEmpty) {
            setState(() {
              mapError = "No results found";
              last_locations = {};
            });
          } else {
            Map<String, List<String>> _locations = {};
            Map<String, List<Map<String, dynamic>>> _all = {};
            // get the last location of each device for static map
            for (Map result in coordinates) {
              _locations['${result["device_id"]}'] = [
                result["gps_latitude"],
                result["gps_longitude"],
              ];
              // get date when GPS was received
              DateTime gpsTimestamp =
                  parserReceivedTime(result["gps_timestamp"]);
              // Get difference between today and GPS date in days
              int diffDays = today.difference(gpsTimestamp).inDays;
              if (diffDays == desiredDiffInDays) {
                if (!_all.keys.contains('${result["device_id"]}')) {
                  _all['${result["device_id"]}'] = [
                    {
                      'name': "Name goes here",
                      'latitude': num.tryParse(result["gps_latitude"]),
                      'longitude': num.tryParse(result["gps_longitude"]),
                    },
                  ];
                } else {
                  _all['${result["device_id"]}'].add(
                    {
                      'name': "Name goes here",
                      'latitude': num.tryParse(result["gps_latitude"]),
                      'longitude': num.tryParse(result["gps_longitude"])
                    },
                  );
                }
              }
            }
            if (_locations.isEmpty || _all.isEmpty) {
              setState(() {
                mapError =
                    "Unable to synthesize data. Probably no data in given duration";
                last_locations = {};
              });
            } else {
              setState(() {
                last_locations = _locations;
                all_locations = _all;
              });
            }
          }
        } else {
          logError("${response.statusCode}", "${response.body}");
          setState(() {
            mapError = "Something went wrong. Please try again later";
            last_locations = {};
          });
        }
      }).catchError((err) {
        setState(() {
          mapError = "Something went wrong. Please try again later";
          last_locations = {};
        });
        print('MAP ERROR!!: $err');
      });
      print('Code executed in ${stopwatch.elapsed}');
    }
  }

  /// Query all the scans from the servers
  void getAllScans() async {
    setState(() => scans = null);
    if (isStringEmpty(apiKey)) {
      showToast("Missing credentials. Please contact administator");
    } else {
      try {
        Map<String, int> _scans = {
          "200": 0,
          "201": 0,
          "202": 0,
          "203": 0,
          "400": 0,
          "403": 0,
          "404": 0,
          "408": 0,
        };
        await http.get(
          '$proxyUrl$sbpUrl/400/qrcode/all',
          headers: {'api_key': apiKey},
        ).then((response) {
          if (response.statusCode == 200) {
            List results = json.decode(response.body);
            if (results.isEmpty) {
              setState(() => scans = "No results found");
            } else {
              for (Map result in results) {
                String key = result["errorcode"];
                if (_scans.keys.contains(key)) {
                  _scans[key] = _scans[key] + 1;
                } else {
                  _scans[key] = 1;
                }
              }
              setState(() => scans = _scans);
            }
          } else {
            logError("${response.statusCode}", "${response.body}");
            showToast("Something went wrong. Please try again later");
            setState(() => scans = "${response.body}");
          }
        });
      } catch (err) {
        showToast("Something went wrong. Please try again later");
        print('SCAN ERROR!!: $err');
        setState(() => scans = "$err");
      }
    }
  }

  /// Here, we shall set up the [Card] that show how verification of scans is faring
  Widget scansPerformance() {
    // add up the number of each scan status
    int total = scans.values.reduce((int a, int b) => a + b);
    int properPercent =
        int.tryParse(((scans["200"] / total) * 100).toStringAsFixed(0));
    int incorrectIDPercent =
        int.tryParse(((scans["400"] / total) * 100).toStringAsFixed(0));
    int incorrectPlacePercent =
        int.tryParse(((scans["403"] / total) * 100).toStringAsFixed(0));
    int incorrectQRPercent =
        int.tryParse(((scans["408"] / total) * 100).toStringAsFixed(0));
    int verifiedPercent =
        int.tryParse(((scans["201"] / total) * 100).toStringAsFixed(0));
    int newPercent =
        int.tryParse(((scans["202"] / total) * 100).toStringAsFixed(0));
    int exceededPercent =
        int.tryParse(((scans["203"] / total) * 100).toStringAsFixed(0));
    return Expanded(
      flex: 1,
      child: Card(
        color: Colors.white,
        child: ListView(
          children: <Widget>[
            SizedBox(height: 20.0),
            Text(
              "QR Scan Performance",
              style: TextStyle(
                color: performCardTextColor,
                fontSize: 20.0,
              ),
              textAlign: TextAlign.center,
            ),
            // spacer
            SizedBox(height: 8.0),
            SizedBox(
              width: MediaQuery.of(context).size.width - 200,
              height: 1.0,
              child: DecoratedBox(
                decoration: BoxDecoration(color: Colors.blueGrey),
              ),
            ),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Proper Businesses",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(Colors.green, properPercent, scans["200"]),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Incorrect Business IDs",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(Colors.red, incorrectIDPercent, scans["400"]),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Incorrect Business Locations",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(
                Color(0xFFEE82EE), incorrectPlacePercent, scans["403"]),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Invalid QR Codes",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(Colors.pink, incorrectQRPercent, scans["408"]),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Business Permits Already Verified",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(Colors.lime, verifiedPercent, scans["201"]),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Newly Registered Business",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(Colors.blue, newPercent, scans["202"]),
            //spacer
            SizedBox(height: 8.0),
            Text(
              "Exceeded Verification Cycle",
              style: TextStyle(color: performCardTextColor),
              textAlign: TextAlign.center,
            ),
            //spacer
            SizedBox(height: 1.0),
            progressIndicator(Colors.yellow, exceededPercent, scans["203"]),
            //spacer
            SizedBox(height: 8.0),
          ],
        ),
      ),
    );
  }

  /// Here, we set the progress indicator widget
  Widget progressIndicator(Color color, int percent, int number) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Container(
            width: 140,
            height: 20.0,
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Container(
                    color: color,
                  ),
                  flex: percent,
                ),
                Flexible(
                  child: Container(
                    color: Colors.white,
                    child: Text(
                      '\t$percent${percent == 0 ? '' : '%'}',
                      style: TextStyle(color: Colors.blueGrey),
                      textAlign: TextAlign.start,
                    ),
                  ),
                  flex: 100 - percent,
                ),
              ],
            ),
            decoration: BoxDecoration(
              border: Border.all(color: Colors.grey),
            ),
            margin: EdgeInsets.symmetric(horizontal: 10.0),
          ),
          flex: 9,
        ),
        Expanded(
          child: Text(
            '$number',
            style: TextStyle(color: Colors.black, fontSize: 12.0),
          ),
          flex: 1,
        ),
      ],
    );
  }

  /// Here, we set up the section showing scan totals
  Widget totals(
      String period, int quantity, int change, bool increase, bool decrease) {
    String text = increase ? 'Up' : decrease ? 'Down' : 'No change';
    change = !increase & !decrease ? 0 : change;
    return Container(
      child: Center(
        child: Column(
          children: <Widget>[
            Row(
              children: [
                MediaQuery.of(context).size.width < 437
                    ? SizedBox(width: 0.0)
                    : Flexible(
                        child: Icon(
                          period == 'Today' ? Icons.today : Icons.date_range,
                          color: Colors.blueGrey,
                          size: 14.0,
                        ),
                      ),
                SizedBox(width: 2.0),
                Expanded(
                  child: Text(
                    period,
                    style: TextStyle(
                      color: Colors.blueGrey,
                      fontSize:
                          MediaQuery.of(context).size.width < 367 ? 12.0 : 14.0,
                    ),
                    textAlign: TextAlign.left,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
            Text(
              '$quantity',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: MediaQuery.of(context).size.width < 600
                    ? MediaQuery.of(context).size.width < 550
                        ? MediaQuery.of(context).size.width < 450
                            ? MediaQuery.of(context).size.width < 350
                                ? 16
                                : 20.0
                            : 30.0
                        : 35.0
                    : 45.0,
                fontWeight: FontWeight.bold,
              ),
              textAlign: TextAlign.start,
            ),
            SizedBox(height: 5.0),
            Row(
              children: [
                Flexible(
                  child: increase
                      ? Icon(Icons.keyboard_arrow_up, color: Colors.green)
                      : decrease
                          ? Icon(Icons.keyboard_arrow_down, color: Colors.red)
                          : Icon(Icons.remove, color: Colors.blueGrey),
                ),
                Expanded(
                  child: Text(
                    '${!increase & !decrease ? '' : '$change%'}${MediaQuery.of(context).size.width < 490 ? '' : ' $text'}',
                    style: TextStyle(
                      color: increase
                          ? Colors.green
                          : decrease ? Colors.red : Colors.blueGrey,
                      fontSize:
                          MediaQuery.of(context).size.width < 367 ? 12.0 : 14.0,
                    ),
                    textAlign: TextAlign.start,
                  ),
                ),
              ],
            ),
            SizedBox(height: 5.0),
          ],
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
        ),
      ),
      margin: EdgeInsets.only(left: 15.0),
    );
  }

  /// General appearance of the user and market sections
  Widget sectionContainer(String section) {
    Widget child;
    switch (section) {
      case "users":
        child = buildUsers();
        break;
      case "markets":
        child = buildMarkets();
        break;
      default:
        child = buildUsers();
        break;
    }
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: MediaQuery.of(context).size.width > 800 ? 0 : 20.0,
      ),
      color: Color(0xFFEEEEEE),
      height: 400,
      padding: EdgeInsets.all(10.0),
      child: child,
    );
  }

  Widget buildUsers() {
    return ListView.builder(
      itemCount: users.length,
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      itemBuilder: (BuildContext context, int index) {
        Map user = users[index];
        return segmentDetails(
          name: '${user["first_name"]} ${user["last_name"]}',
          id: '${user["user_id"]}',
          role: '${user["user_role"]}',
          active: user["active"],
        );
      },
    );
  }

  Widget buildMarkets() {
    return ListView.builder(
      itemCount: markets.length,
      scrollDirection: Axis.horizontal,
      padding: EdgeInsets.symmetric(horizontal: 5.0),
      itemBuilder: (BuildContext context, int index) {
        Map market = markets[index];
        return segmentDetails(
          name: '${market["market_name"]}',
          id: '${market["market_id"]}',
          role: "Market",
        );
      },
    );
  }

  /// Style the number of scans segment in each part
  Widget noOfScansPerSegment(Color color, int number) {
    return Row(
      children: [
        Flexible(
          child: Container(
            decoration: BoxDecoration(
              border: Border.all(
                color: Colors.grey,
                width: 2.0,
              ),
              color: color,
            ),
            width: 30.0,
            height: 10.0,
            padding: EdgeInsets.all(3.0),
          ),
          flex: 1,
        ),
        Padding(
          child: Text(
            '$number',
            style: TextStyle(
              fontSize: 14.0,
              color: Colors.blueGrey,
            ),
          ),
          padding: EdgeInsets.only(left: 12.0),
        ),
      ],
    );
  }

  /// Here, we create the [Card] that lays out the particulars on clerks/markets
  Widget segmentDetails({
    String name,
    String id,
    String role,
    bool active = true,
  }) {
    return Container(
      child: Card(
        color: Colors.white,
        elevation: 0.0,
        child: Container(
          margin: EdgeInsets.only(top: 5.0),
          padding: EdgeInsets.all(15.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child: Padding(
                    child: Text(
                      name ?? 'Name Goes here',
                      style: TextStyle(
                        color: Colors.blueGrey,
                        fontSize: 20.0,
                      ),
                      textAlign: TextAlign.start,
                    ),
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                  ),
                  flex: 1),
              SizedBox(
                width: MediaQuery.of(context).size.width - 50,
                height: 2,
                child: DecoratedBox(
                  decoration: BoxDecoration(
                    color: Color(0xFFEEEEEE),
                  ),
                ),
              ),
              Expanded(
                child: Padding(
                  child: Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(right: 10.0),
                      ),
                      Container(
                        width: 50.0,
                        height: 50.0,
                        margin: EdgeInsets.only(left: 2.0),
                        decoration: BoxDecoration(
                          border: Border.all(
                            color: Colors.white,
                            width: 2.0,
                          ),
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(
                              userProfilePicture('${id ?? 'Nothing'}', '128'),
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(right: 50.0),
                      ),
                      Column(
                        children: <Widget>[
                          Text(
                            id ?? 'ID Goes Here',
                            style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 18.0,
                            ),
                            textAlign: TextAlign.start,
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 5.0),
                          ),
                          Text(
                            role ?? 'Role Goes Here',
                            style: TextStyle(
                              color: Colors.blueGrey,
                              fontSize: 18.0,
                            ),
                            textAlign: TextAlign.start,
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 5.0),
                          ),
                          Row(
                            children: <Widget>[
                              FadeTransition(
                                opacity: _animationController,
                                child: Container(
                                  width: 10.0,
                                  height: 10.0,
                                  decoration: BoxDecoration(
                                    color: active
                                        ? Colors.green
                                        : Colors.redAccent,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                              SizedBox(width: 5.0),
                              Text(
                                active ? 'Active' : 'Disabled',
                                style: TextStyle(
                                  color: Colors.blueGrey,
                                  fontSize: 18.0,
                                ),
                                textAlign: TextAlign.start,
                              ),
                            ],
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.start,
                      ),
                    ],
                  ),
                  padding: EdgeInsets.only(bottom: 10.0, top: 15.0),
                ),
                flex: 2,
              ),
              Padding(
                child: SizedBox(
                  width: MediaQuery.of(context).size.width - 50,
                  height: 2,
                  child: DecoratedBox(
                    decoration: BoxDecoration(
                      color: Color(0xFFEEEEEE),
                    ),
                  ),
                ),
                padding: EdgeInsets.symmetric(vertical: 10.0),
              ),
              Expanded(
                child: Padding(
                  child: Row(
                    children: <Widget>[
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            noOfScansPerSegment(Colors.green, 50),
                            SizedBox(height: 15.0),
                            noOfScansPerSegment(Colors.red, 50),
                            SizedBox(height: 15.0),
                            noOfScansPerSegment(Color(0xFFEE82EE), 50),
                            SizedBox(height: 15.0),
                            noOfScansPerSegment(Colors.pink, 50),
                            SizedBox(height: 15.0),
                          ],
                          crossAxisAlignment: CrossAxisAlignment.center,
                        ),
                      ),
                      Expanded(
                        child: VerticalDivider(color: Colors.black),
                      ),
                      Expanded(
                        child: Column(
                          children: <Widget>[
                            noOfScansPerSegment(Colors.lime, 20),
                            SizedBox(height: 15.0),
                            noOfScansPerSegment(Colors.blue, 20),
                            SizedBox(height: 15.0),
                            noOfScansPerSegment(Colors.yellow, 20),
                            SizedBox(height: 15.0),
                          ],
                          crossAxisAlignment: CrossAxisAlignment.center,
                        ),
                      ),
                    ],
                  ),
                  padding: EdgeInsets.all(10.0),
                ),
                flex: 3,
              ),
            ],
          ),
        ),
      ),
      width: 300.0,
    );
  }

  /// Here, we set up the segment headers,
  /// consisting of the title and period under review
  Widget segmentHeader(String title, Function cb, String val) {
    return Container(
      child: Row(
        children: <Widget>[
          Expanded(
            child: Text(
              '${MediaQuery.of(context).size.width < 440 ? '' : '\t'}$title',
              style: TextStyle(
                color: Colors.blueGrey,
                fontSize: MediaQuery.of(context).size.width < 560 ? 20.0 : 26.0,
              ),
              textAlign: TextAlign.start,
            ),
            flex: 1,
          ),
          Expanded(
            child: Container(
              margin: const EdgeInsets.symmetric(horizontal: 30.0),
              padding: const EdgeInsets.all(10.0),
              decoration: BoxDecoration(
                border: Border.all(
                  width: 0.5,
                  color: Colors.grey,
                ),
              ),
              child: roleDropDown(cb, val),
            ),
            flex: 1,
          ),
        ],
      ),
      margin: EdgeInsets.only(top: 0.0, bottom: 10.0),
      width: 200.0,
    );
  }

  Widget roleDropDown(Function cb, String val) => Container(
        padding: MediaQuery.of(context).size.width < 978
            ? EdgeInsets.symmetric(vertical: 10.0)
            : EdgeInsets.symmetric(horizontal: 5.0),
        child: Container(
          child: DropdownButton<String>(
            isExpanded: true,
            hint: Text("Please select a role"),
            items: durations
                .map(
                  (item) => DropdownMenuItem(
                    value: item,
                    child: Text(
                      item,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                )
                .toList(),
            onChanged: (value) => cb(value),
            value: val,
          ),
        ),
      );
}
