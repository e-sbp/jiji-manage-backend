import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'register_market.dart';
import 'title.dart';
import 'extras.dart';
import 'urls.dart';

class EditMarketScreen extends StatefulWidget {
  final String token;

  EditMarketScreen(this.token);

  _EditMarketScreenState createState() => _EditMarketScreenState();
}

class _EditMarketScreenState extends State<EditMarketScreen> {
  List<dynamic> markets = [];

  @override
  void initState() {
    super.initState();
    loadMarkets();
  }

  void loadMarkets() async {
    setState(() => markets = []);
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => markets = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/markets/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() {
            markets = json.decode(response.body);
            landingState.setState(() => landingState.refresh = loadMarkets);
          });
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => markets = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => markets = []);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Color(0xFFEEEEEE),
        ),
        SingleChildScrollView(
          controller: landingState.controller,
          child: markets.isEmpty
              ? Container(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 30.0),
                        Text(
                          "Waiting for markets...",
                          style: TextStyle(color: Colors.black, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                  height: MediaQuery.of(context).size.height / 2,
                )
              : Container(
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width < 700
                          ? MediaQuery.of(context).size.width - 10
                          : MediaQuery.of(context).size.width - 200,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(
                        vertical: 18.0,
                        horizontal: 5.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 25.0),
                      child: ListView(
                        children: [
                          SizedBox(height: 1.0),
                          Column(
                            children: <Widget>[
                              Image.asset(
                                'images/ic_barcode.jpg',
                                scale: 2.0,
                              ),
                              SizedBox(height: 10.0),
                              Material(
                                child: Text(
                                  'E-SBP',
                                  style: TextStyle(
                                    fontSize: 24.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                          // spacer
                          SizedBox(height: 10.0),
                          Container(
                            margin: const EdgeInsets.all(30.0),
                            padding: const EdgeInsets.all(1.0),
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.black,
                              ),
                            ),
                            child: tableView(),
                            width: MediaQuery.of(context).size.width,
                          ),
                          // spacer
                          SizedBox(height: 10.0),
                        ],
                      ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  /// Create the table of markets
  Widget tableView() {
    List<TableRow> _children = [
      _buildTableHeaders('Market ID,Market Name,User ID,Edit', 16.0)
    ];
    for (int i = 0; i < markets.length; i++) {
      _children.add(_buildTableDetails(markets[i], 14.0));
    }
    return Table(
      defaultColumnWidth: FixedColumnWidth(
        MediaQuery.of(context).size.width > 460 ? 50.0 : 30.0,
      ),
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        verticalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: _children,
    );
  }

  /// Set up the table column titles
  TableRow _buildTableHeaders(String columnNames, num fontSize) {
    return TableRow(
      children: columnNames.split(',').map((name) {
        return Container(
          alignment: Alignment.center,
          child: Text(
            name,
            style: TextStyle(fontSize: fontSize, color: Colors.black),
          ),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }

  /// Set up the rows containing market details
  TableRow _buildTableDetails(Map market, num fontSize) {
    return TableRow(
      children: [
        Container(
          alignment: Alignment.center,
          child: Text(
            market['market_id'].toString(),
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            market['market_name'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            market['user_id'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          child: IconButton(
            icon: Icon(Icons.edit, color: Colors.black),
            onPressed: () => _editMarket(market),
          ),
        ),
      ],
    );
  }

  void _editMarket(Map device) {
    Map<String, String> details = {
      'market_id': '${device['market_id']}',
      "market_name": device["market_name"],
      "user_id": device["user_id"],
    };
    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppTitle(() => Navigator.of(context).pop()),
        body: RegisterMarketScreen(widget.token, details: details),
      );
    }));
  }
}
