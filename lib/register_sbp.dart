import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:html';
import 'dart:js' as js;

import 'landing.dart' show landingState;
import 'accent_override.dart';
import 'colors.dart';
import 'extras.dart';
import 'urls.dart';

class RegisterSBPScreen extends StatefulWidget {
  final String token;
  final Map<String, String> details;

  RegisterSBPScreen(this.token, {this.details});

  _RegisterSBPScreenState createState() => _RegisterSBPScreenState();
}

class _RegisterSBPScreenState extends State<RegisterSBPScreen> {
  String businessName = '', plotNumber = '', physicalAddress = '';
  String phone = '', emailAddress = '', market, activity = '', marketName = '';
  Map<String, String> details = {};
  List<dynamic> markets = [];

  final businessNameController = TextEditingController();
  final plotNumberController = TextEditingController();
  final physicalAddressController = TextEditingController();
  final phoneController = TextEditingController();
  final emailController = TextEditingController();
  final activityController = TextEditingController();

  num opacity = 1.0;

  @override
  void initState() {
    super.initState();
    details = widget.details ?? {};
    businessName = details['business_name'] ?? '';
    plotNumber = details['plot_number'] ?? '';
    physicalAddress = details['physical_address'] ?? '';
    market = details['market_id'];
    phone = details['mssdn'] ?? '';
    emailAddress = details['mail_address'] ?? '';
    activity = details['activity'] ?? '';

    businessNameController.text = businessName;
    plotNumberController.text = plotNumber;
    physicalAddressController.text = physicalAddress;
    phoneController.text = phone;
    emailController.text = emailAddress;
    activityController.text = activity;

    loadMarkets();
    restoreScreenFromJavascript();
  }

  var div = DivElement()
    ..id = 'insert-here'
    ..style.width = '100%'
    ..style.height = '100%'
    ..style.color = '#000';

  @override
  void dispose() {
    super.dispose();
    landingState.setState(() => landingState.opacity = 1.0);
    if (div != null) {
      div.childNodes.clear();
      div.children.clear();
      document.body.children.remove(div);
    }
  }

  void _businessName(String str) {
    if (!mounted) return;
    setState(() => businessName = str.trim());
  }

  void _plotNumber(String str) {
    if (!mounted) return;
    setState(() => plotNumber = str.trim());
  }

  void _physicalAddress(String str) {
    if (!mounted) return;
    setState(() => physicalAddress = str.trim());
  }

  void _email(String str) {
    if (!mounted) return;
    setState(() => emailAddress = str.toLowerCase().trim());
  }

  void _activity(String str) {
    if (!mounted) return;
    setState(() => activity = str.trim());
  }

  void _phone(String str) {
    if (!mounted) return;
    setState(() => phone = str.toLowerCase().trim());
  }

  void _market(String str) {
    if (!mounted) return;
    setState(() => market = str.trim());
  }

  void loadMarkets() async {
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => markets = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/markets/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() => markets = json.decode(response.body));
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => markets = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => markets = []);
      });
    }
  }

  Future<dynamic> handleRegister(Map<String, String> value) async {
    if (isStringEmpty(widget.token) ||
        isStringEmpty(apiKey) ||
        (details.isNotEmpty & isStringEmpty(details["business_id"]))) {
      return "Something missing. Please try again later";
    }
    var response;
    if (details.isEmpty) {
      response = await http.post(
        '$proxyUrl$sbpUrl/200/register/qrcode/',
        headers: {
          'api_key': apiKey,
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    } else {
      response = await http.post(
        '$proxyUrl$sbpUrl/200/edit/sbp/${details["business_id"]}/',
        headers: {
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    }
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _registerUser() {
    if (!mounted) return;
    for (Map val in markets) {
      if ('${val["market_id"]}' == market) {
        setState(() => marketName = '${val["market_name"]}');
        break;
      }
    }
    // render HTML so as to download new or updated sbp permit
    landingState.setState(() => landingState.opacity = 0.0);
    setState(() => opacity = 0.0);
    document.body.children.add(div);
    HttpRequest.getString("/permit.html").then((resp) {
      // for (String str in sbp_details.keys) {
      //   resp = resp.replaceAll("{{ $str }}", sbp_details[str]);
      // }
      div.appendHtml(resp, treeSanitizer: NodeTreeSanitizer.trusted);
    });
    // if (isStringEmpty(plotNumber)) {
    //   showToast("Please enter the plot number");
    // } else if (isStringEmpty(physicalAddress)) {
    //   showToast("Please enter the physical address");
    // } else if (isStringEmpty(businessName)) {
    //   showToast("Please enter the business name");
    // } else if (isStringEmpty(emailAddress)) {
    //   showToast("Please enter the email address");
    // } else if (!validateEmail(emailAddress)) {
    //   showToast("Please enter a valid email address");
    // } else if (isStringEmpty(phone)) {
    //   showToast("Please enter the phone number");
    // } else if (isStringEmpty(market)) {
    //   showToast("Please select a market");
    // } else {
    //   showToast("Please wait...");
    //   handleRegister({
    //     "business_name": businessName,
    //     "plot_number": plotNumber,
    //     "mail_address": emailAddress,
    //     "physical_address": physicalAddress,
    //     "mssdn": "254${int.tryParse(phone)}",
    //     "status": "202",
    //     "market_id": market,
    //     "amount": "2000",
    //     "activity": activity,
    //     "latitude": "${center["latitude"]}",
    //     "longitude": "${center["longitude"]}",
    //   }).then((res) {
    //     if (res is String) {
    //       showToast(res);
    //       return;
    //     }
    //     final errorCode = res['errorCode'];
    //     final desc = res['desc'];
    //     if (desc == null || errorCode == null) {
    //       showToast("Error while registering in");
    //       return;
    //     }
    //     showToast("$desc");
    //     print("$errorCode");
    //     if (int.parse(errorCode) == 200) {
    //       Map<String, String> sbp_details = {
    //         "permit_id": '${res["PermitID"]}'.toUpperCase(),
    //         "verification_cycle": '${res["VerificationCycle"]}',
    //         "business_name": businessName.toUpperCase(),
    //         "plot_number": plotNumber.toUpperCase(),
    //         "business_id": "${res["BusinessID"]}",
    //         "market_id": market,
    //         "market_name":
    //             !isStringEmpty(marketName) ? marketName : "Market Name missing",
    //         "permit_fee": '${res["PermitFee"]}',
    //         "email_address": emailAddress,
    //         "business_address": physicalAddress,
    //         "activity": '${res["Activity"]}',
    //         "data": '${res["QRCode"]}',
    //       };

    //       // render HTML so as to download new or updated sbp permit
    //       landingState.setState(() => landingState.opacity = 0.0);
    //       setState(() => opacity = 0.0);
    //       document.body.children.add(div);
    //       HttpRequest.getString("/permit.html").then((resp) {
    //         // for (String str in sbp_details.keys) {
    //         //   resp = resp.replaceAll("{{ $str }}", sbp_details[str]);
    //         // }
    //         div.appendHtml(resp, treeSanitizer: NodeTreeSanitizer.trusted);
    //       });
    //     }
    //   }).catchError((err) {
    //     showToast("Something went wrong. Please try again later");
    //     print("$err");
    //   });
    // }
  }

  /// restore slide menu visibility and go to SBPs page or home page
  /// after clearing permit html when the permit PDF has been downloaded
  void restoreScreenFromJavascript() {
    // Here, we enable JS to call Dart code by runnning `restore` function
    js.context["restore"] = () {
      landingState.setState(() {
        landingState.opacity = 1.0;
        // if we were registrating sbp, go home
        if (details.isEmpty) {
          landingState.screen = "home";
          landingState.refresh = null;
        }
      });
      // if we were editing SBP, go to list of SBPs
      if (details.isNotEmpty) Navigator.of(context).pop();
    };
  }

  @override
  Widget build(BuildContext context) {
    return Opacity(
      opacity: opacity,
      child: Stack(
        children: [
          Container(
            color: Color(0xFFEEEEEE),
          ),
          SingleChildScrollView(
            controller: landingState.controller,
            child: markets.isEmpty
                ? Container(
                    child: Center(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CircularProgressIndicator(),
                          SizedBox(height: 30.0),
                          Text(
                            "Waiting for markets...",
                            style:
                                TextStyle(color: Colors.black, fontSize: 16.0),
                          ),
                        ],
                      ),
                    ),
                    height: MediaQuery.of(context).size.height / 2,
                  )
                : Container(
                    child: Center(
                      child: Container(
                        color: Colors.white,
                        margin: EdgeInsets.symmetric(
                            vertical: 18.0, horizontal: 15.0),
                        child: Column(
                          children: <Widget>[
                            // Given a child, this widget forces child to have a specific width and/or height
                            // else it will adjust itself to given dimension
                            SizedBox(height: 1.0),
                            // display children in vertical array
                            Column(
                              children: <Widget>[
                                Image.asset(
                                  'images/ic_barcode.jpg',
                                  scale: 2.0,
                                ),
                                SizedBox(height: 10.0),
                                Material(
                                  child: Text(
                                    'E-SBP',
                                    style: TextStyle(
                                      fontSize: 24.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            Container(
                              width: MediaQuery.of(context).size.width / 2 + 80,
                              child: Row(
                                children: <Widget>[
                                  // Plot Number
                                  Flexible(
                                    child: AccentOverride(
                                      child: TextField(
                                        controller: plotNumberController,
                                        decoration: InputDecoration(
                                          labelText: 'Plot Number',
                                          border: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.black),
                                          ),
                                        ),
                                        onChanged: _plotNumber,
                                      ),
                                    ),
                                  ),
                                  Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 10.0)),
                                  // Physical Address
                                  Flexible(
                                    child: AccentOverride(
                                      child: TextField(
                                        controller: physicalAddressController,
                                        decoration: InputDecoration(
                                          labelText: 'Physical Address',
                                          border: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.black),
                                          ),
                                        ),
                                        onChanged: _physicalAddress,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            Container(
                              width: MediaQuery.of(context).size.width / 2 + 80,
                              child: Row(
                                children: <Widget>[
                                  // Business Name
                                  Expanded(
                                    child: AccentOverride(
                                      child: TextField(
                                        enabled: details.isEmpty ? true : false,
                                        decoration: InputDecoration(
                                          labelText: 'Business Name',
                                          border: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.black),
                                          ),
                                        ),
                                        onChanged: _businessName,
                                        controller: businessNameController,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            Container(
                              width: MediaQuery.of(context).size.width / 2 + 80,
                              child: Row(
                                children: <Widget>[
                                  // Email Address
                                  Expanded(
                                    child: AccentOverride(
                                      child: TextField(
                                        enabled: details.isEmpty ? true : false,
                                        decoration: InputDecoration(
                                          labelText: 'Email Address',
                                          border: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.black),
                                          ),
                                        ),
                                        onChanged: _email,
                                        controller: emailController,
                                        keyboardType:
                                            TextInputType.emailAddress,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            Container(
                              width: MediaQuery.of(context).size.width / 2 + 80,
                              child: Row(
                                children: <Widget>[
                                  MediaQuery.of(context).size.width >= 250
                                      ? Expanded(
                                          flex: 1,
                                          child: Row(
                                            children: <Widget>[
                                              Image.asset('images/ke_flag.png',
                                                  scale: 1.2),
                                              MediaQuery.of(context)
                                                          .size
                                                          .width >=
                                                      780
                                                  ? Text(' (+254)',
                                                      style: TextStyle(
                                                          color: Colors.black))
                                                  : SizedBox(
                                                      width: 0.0, height: 0.0),
                                            ],
                                          ),
                                        )
                                      : Container(),
                                  Padding(
                                      padding: EdgeInsets.symmetric(
                                          horizontal: 2.0)),
                                  // Phone number
                                  Expanded(
                                    flex: 4,
                                    child: AccentOverride(
                                      child: TextField(
                                        controller: phoneController,
                                        decoration: InputDecoration(
                                          labelText: 'Phone number',
                                          helperText: "Don't start with 0",
                                          helperStyle: TextStyle(
                                            color: Colors.black,
                                            fontSize: 12.0,
                                          ),
                                          border: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.black),
                                          ),
                                        ),
                                        keyboardType: TextInputType.phone,
                                        onChanged: _phone,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            Container(
                              child: Column(
                                children: [
                                  // spacer
                                  SizedBox(height: 10.0),
                                  roleDropDown(),
                                ],
                              ),
                              width: MediaQuery.of(context).size.width / 2 + 80,
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            Container(
                              width: MediaQuery.of(context).size.width / 2 + 80,
                              child: Row(
                                children: <Widget>[
                                  // Business activity
                                  Expanded(
                                    child: AccentOverride(
                                      child: TextField(
                                        decoration: InputDecoration(
                                          labelText:
                                              'Describe your business activity eg Mitumba or MPESA',
                                          border: UnderlineInputBorder(
                                            borderSide:
                                                BorderSide(color: Colors.black),
                                          ),
                                        ),
                                        onChanged: _activity,
                                        controller: activityController,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            // spacer
                            SizedBox(height: 10.0),
                            // Places the buttons horizontally according to the padding
                            Container(
                              child: Row(
                                children: <Widget>[
                                  RaisedButton(
                                    child: Text(
                                      'Submit',
                                      style: TextStyle(
                                        color: myBackgroundColor,
                                        fontSize: 15.0,
                                      ),
                                    ),
                                    onPressed: () => _registerUser(),
                                    shape: StadiumBorder(),
                                    color: Colors.green,
                                    elevation: 8.0,
                                  ),
                                ],
                                mainAxisAlignment: MainAxisAlignment.end,
                              ),
                              width: MediaQuery.of(context).size.width / 2 + 80,
                            ),
                          ],
                        ),
                        width: MediaQuery.of(context).size.width,
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }

  Widget roleDropDown() => Container(
        padding: MediaQuery.of(context).size.width < 978
            ? EdgeInsets.symmetric(vertical: 10.0)
            : EdgeInsets.symmetric(horizontal: 5.0),
        child: Container(
          child: Row(
            children: [
              Icon(Icons.business_center),
              Flexible(
                child: DropdownButton(
                  isExpanded: true,
                  hint: Text("Please select a market"),
                  items: markets.map((value) {
                    return value is Map
                        ? DropdownMenuItem<String>(
                            value: '${value['market_id']}',
                            child: Text(
                              '${value['market_name']}',
                            ),
                          )
                        : DropdownMenuItem<String>(
                            value: value,
                            child: Text('$value'),
                          );
                  }).toList(),
                  onChanged: (value) => _market(value),
                  value: market,
                ),
                // margin: EdgeInsets.symmetric(horizontal: 10.0),
              ),
            ],
          ),
        ),
      );
}
