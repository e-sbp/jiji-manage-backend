/// Library that allows one to display progress widgets based on percentage,
/// can be Circular or Linear, you can also customize it to one's needs.
/// Code can be found at https://github.com/diegoveloper/flutter_percent_indicator
/// and example is in 'example' folder

library percent_indicator;

export 'circular_percent_indicator.dart';
export 'linear_percent_indicator.dart';
