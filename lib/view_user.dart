import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'extras.dart';
import 'urls.dart';

class ViewUserScreen extends StatefulWidget {
  final String token;

  ViewUserScreen(this.token);

  _ViewUserScreenState createState() => _ViewUserScreenState();
}

class _ViewUserScreenState extends State<ViewUserScreen> {
  List<dynamic> users = [];

  @override
  void initState() {
    super.initState();
    loadUsers();
  }

  void loadUsers() async {
    setState(() => users = []);
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => users = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/users/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() {
            users = json.decode(response.body);
            landingState.setState(() => landingState.refresh = loadUsers);
          });
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => users = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => users = []);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Color(0xFFEEEEEE),
        ),
        SingleChildScrollView(
          controller: landingState.controller,
          child: users.isEmpty
              ? Container(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 30.0),
                        Text(
                          "Waiting for users...",
                          style: TextStyle(color: Colors.black, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                  height: MediaQuery.of(context).size.height / 2,
                )
              : Container(
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width - 100,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(
                        vertical: 18.0,
                        horizontal: 5.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 25.0),
                      child: MediaQuery.of(context).size.width < 920
                          ? user()
                          : ListView(
                              children: [
                                SizedBox(height: 1.0),
                                Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'images/ic_barcode.jpg',
                                      scale: 2.0,
                                    ),
                                    SizedBox(height: 10.0),
                                    Material(
                                      child: Text(
                                        'E-SBP',
                                        style: TextStyle(
                                          fontSize: 24.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                // spacer
                                SizedBox(height: 10.0),
                                Container(
                                  margin: const EdgeInsets.all(30.0),
                                  padding: const EdgeInsets.all(1.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: Colors.black,
                                    ),
                                  ),
                                  child: tableView(),
                                  width: MediaQuery.of(context).size.width,
                                ),
                                // spacer
                                SizedBox(height: 10.0),
                              ],
                            ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  /// Create the table of users, with possible changes
  Widget tableView() {
    List<TableRow> _children = [
      _buildTableHeaders(
        'Profile Picture,ID,First Name,Last Name,Email Address,Phone Number',
        16.0,
      )
    ];
    for (int i = 0; i < users.length; i++) {
      _children.add(_buildTableDetails(users[i], 14.0));
    }
    return Table(
      defaultColumnWidth: FixedColumnWidth(
        MediaQuery.of(context).size.width > 460 ? 80.0 : 30.0,
      ),
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        verticalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: _children,
    );
  }

  /// Set up the table column titles
  TableRow _buildTableHeaders(String columnNames, num fontSize) {
    return TableRow(
      children: columnNames.split(',').map((name) {
        return Container(
          alignment: Alignment.center,
          child: Text(
            name,
            style: TextStyle(fontSize: fontSize, color: Colors.black),
          ),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }

  /// Set up the rows containing user details
  TableRow _buildTableDetails(Map user, num fontSize) {
    return TableRow(
      children: [
        Container(
          alignment: Alignment.center,
          child: Image.network(
            userProfilePicture(user['user_id'] ?? '', '32'),
            scale: 0.9,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['user_id'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['first_name'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['last_name'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['email_address'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            '${user['phone_number']}'.replaceFirst('254', '0'),
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
      ],
    );
  }

  /// Screen for small devices
  Widget user() {
    num textSize = MediaQuery.of(context).size.width > 475 ? 16.0 : 14.0;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: MediaQuery.of(context).size.width < 800 ? 1 : 2,
      ),
      itemCount: users.length,
      itemBuilder: (context, index) {
        return Container(
          child: Card(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(15.0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Container(
                      width: 50.0,
                      height: 50.0,
                      margin: EdgeInsets.only(left: 2.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            userProfilePicture(users[index]['user_id'], '32'),
                            scale: 0.75,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          color: users[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            users[index]["first_name"],
                            style: TextStyle(
                              color: users[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.person_outline,
                          color: users[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            users[index]["last_name"],
                            style: TextStyle(
                              color: users[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.person_pin_circle,
                          color: users[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            users[index]["user_id"],
                            style: TextStyle(
                              color: users[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.phone,
                          color: users[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            '+${users[index]["phone_number"]}',
                            style: TextStyle(
                              color: users[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.email,
                          color: users[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            users[index]["email_address"],
                            style: TextStyle(
                              color: users[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                  ],
                ),
              ),
            ),
            elevation: 3.0,
          ),
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        );
      },
    );
  }
}
