import 'package:flutter/material.dart';

import 'extras.dart';
import 'urls.dart';
import 'map.dart' as map;

class StaticMapsProvider extends StatefulWidget {
  final String token;
  final String username;
  final String color;
  final String size;
  final String zoom;
  final Map<String, List<String>> last_locations;
  final Map<String, List<Map<String, dynamic>>> all_locations;

  StaticMapsProvider(
    this.token,
    this.username,
    this.last_locations, {
    @required this.all_locations,
    this.color = 'blue',
    this.zoom = "15",
    this.size = "400x400",
  });

  @override
  _StaticMapsProviderState createState() => _StaticMapsProviderState();
}

class _StaticMapsProviderState extends State<StaticMapsProvider> {
  String url;
  bool isMapLoaded = false;

  @override
  void initState() {
    super.initState();
  }

  String constructMarkers(Map<String, List<String>> markers) {
    if (markers.isEmpty) {
      return "";
    }
    String url =
        'https://maps.googleapis.com/maps/api/staticmap?center=${center['latitude']},${center['longitude']}' +
            '&zoom=${widget.zoom}&size=${widget.size}&scale=2' +
            '&markers=size:mid%7Ccolor:${widget.color}%7Clabel:H%7C${center['latitude']},${center['longitude']}';
    for (String deviceID in markers.keys) {
      url = "$url&markers=size:tiny%7C" +
          "color:0x${stringToColour(markers[deviceID][0]).replaceFirst('#', '')}%7C" +
          "label:${deviceID}%7C" +
          "${markers[deviceID][0]},${markers[deviceID][1]}";
    }
    url = "$url&key=${googleMapsApiKey}";
    return url;
  }

  @override
  Widget build(BuildContext context) {
    return widget.last_locations.isNotEmpty
        ? staticMapScreen()
        : loadingWidget();
  }

  Widget staticMapScreen() {
    String url = constructMarkers(widget.last_locations);
    return isStringEmpty(url)
        ? loadingWidget()
        : GestureDetector(
            child: Container(
              decoration: BoxDecoration(
                color: Colors.grey,
                border: Border.all(color: Colors.grey[200]),
              ),
              child: Image.network(url, fit: BoxFit.fitWidth),
            ),
            onTap: () {
              Navigator.of(context).push(
                MaterialPageRoute(
                  builder: (_) => map.MapScreen(
                    widget.all_locations,
                    zoom: 13.0,
                  ),
                ),
              );
            });
  }
}
