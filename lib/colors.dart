/// define all colors here
import 'package:flutter/material.dart';

const myAccentColor = Color(0xFF03A9F4);
const myPrimaryColor = Color(0xFF2196F3);
const myDarkPrimaryColor = Color(0xFF1976D2);
const myLightPrimaryColor = Color(0xFFBBDEFB);
const myBackgroundColor = Color(0xFFFFFFFF);
const myPrimaryTextColor = Color(0xFF212121);
const mySecondaryTextColor = Color(0xFF757575);
const myErrorColor = Color(0xFFE53935);
const myDividerColor = Color(0xFFBDBDBD);
const myLoginColor = Color(0xFF00FF00);
const colorCodes = {
  "200": Colors.green,
  "400": Colors.red,
  "403": Color(0xFFEE82EE),
  "408": Colors.pink,
  "201": Colors.lime,
  "202": Colors.blue,
  "203": Colors.yellow,
};
