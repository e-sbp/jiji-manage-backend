import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:html';
import 'dart:async';
import 'dart:convert';

import 'accent_override.dart';
import 'landing.dart';
import 'colors.dart';
import 'extras.dart';
import 'urls.dart';
import 'title.dart';

class LoginScreen extends StatefulWidget {
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  String name = '', password = '';

  @override
  void initState() {
    print('Logged in');
    // register callback with document.onKeyPress function
    // so that keys that are pressed are passed from browser to app
    document.onKeyPress.listen(keyPressed);
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _name(String str) {
    if (!mounted) return;
    setState(() => name = str.toLowerCase().trim());
  }

  void _password(String str) {
    if (!mounted) return;
    setState(() => password = str);
  }

  Future<dynamic> handleSignIn(String username, String password) async {
    final response = await http.get(
      '$proxyUrl$baseUrl/100/user/login/',
      headers: {
        'user_name': username,
        'pin': password,
        'api_key': await apiKey,
        'device_id': "1",
      },
    );
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _login(BuildContext context) {
    if (!mounted) return;
    setState(() {
      if (isStringEmpty(name)) {
        showToast("Please enter your username");
      } else if (isStringEmpty(password)) {
        showToast("Please enter your password");
      } else {
        showToast('Please wait...');
        try {
          handleSignIn(name, password).then((res) {
            final errorCode = res['errorCode'];
            final token = res['token'];
            final role = res['user_role'];
            final user_id = res['user_id'];
            String full = res['name'] ?? "";
            if (errorCode == null) {
              showToast("Error while logging in");
              logError("ERROR", "Error code missing");
              return;
            }
            if (int.tryParse(errorCode) == 200) {
              if (token == null) {
                showToast("Error while logging in");
                logError("ERROR", "Token missing");
                return;
              }
              showToast("Success");
              Navigator.of(context).pushReplacement(
                MaterialPageRoute(
                  builder: (_) =>
                      LandingPage(token, name, '$user_id', role, full),
                ),
              );
            } else {
              logError("LOGIN ERROR", "Error: $errorCode");
              showToast("Failed login: ${res["desc"]}");
            }
          });
        } catch (err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        }
      }
    });
  }

  /// This function will detect when a key is pressed by user
  void keyPressed(KeyboardEvent event) {
    // link key press event to specific app control eg Enter to login
    if (event.keyCode == KeyCode.ENTER) _login(context);
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppTitle(() => logout(context, '', ''), loggedIn: false),
        body: Stack(
          children: [
            Container(
              color: Color(0xFFEEEEEE),
            ),
            Container(
              child: MediaQuery.of(context).size.width < 1024
                  ? Row(children: [loginForm()])
                  : Row(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        loginForm(),
                        Expanded(
                          child: Container(
                            margin: EdgeInsets.symmetric(vertical: 8.0),
                            child: Column(
                              children: <Widget>[
                                Expanded(
                                  child: Image.asset(
                                    "images/mombasa.jpg",
                                    scale: 1.5,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              ],
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                            ),
                          ),
                        ),
                      ],
                    ),
              margin: EdgeInsets.all(50.0),
            ),
          ],
        ),
      ),
      // handle back press
      onWillPop: () => _confirmPop(context),
    );
  }

  /// This method is called when the user is on login screen & presses the back button
  Future<bool> _confirmPop(BuildContext context) {
    showToast("Good Bye");
    Navigator.pop(context);
    return Future.value(true);
  }

  Widget loginForm() {
    return Expanded(
      child: Center(
        child: Container(
          color: Colors.white,
          margin: EdgeInsets.symmetric(vertical: 8.0),
          padding: EdgeInsets.symmetric(horizontal: 30.0, vertical: 0.0),
          // width: MediaQuery.of(context).size.width / 2,
          child: ListView(
            children: <Widget>[
              // Given a child, this widget forces child to have a specific width and/or height
              // else it will adjust itself to given dimension
              SizedBox(height: 5.0),
              // display children in vertical array
              Column(
                children: <Widget>[
                  Image.asset(
                    'images/ic_barcode.jpg',
                    scale: 2.0,
                  ),
                  SizedBox(height: 5.0),
                  Material(
                    child: Text(
                      'e-SBP',
                      style: TextStyle(
                        fontSize: 20.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 20.0),
              // let user enter text
              AccentOverride(
                child: Container(
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border.all(
                      color: Colors.grey,
                      width: 0.5,
                    ),
                  ),
                  child: TextField(
                    autofocus: true,
                    decoration: InputDecoration(
                      labelText: 'Username',
                      filled: true,
                      border: InputBorder.none,
                    ),
                    onChanged: _name,
                  ),
                  width: MediaQuery.of(context).size.width / 2,
                ),
              ),
              SizedBox(height: 10.0),
              // spacer
              // Password
              AccentOverride(
                child: Container(
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Password',
                      filled: true,
                      border: InputBorder.none,
                    ),
                    obscureText: true,
                    onChanged: _password,
                  ),
                  decoration: BoxDecoration(
                    shape: BoxShape.rectangle,
                    border: Border.all(
                      color: Colors.grey,
                      width: 0.5,
                    ),
                  ),
                ),
              ),
              // Places the buttons horizontally according to the padding
              ButtonBar(
                children: <Widget>[
                  RaisedButton(
                    child: Text(
                      'NEXT',
                      style: TextStyle(
                          color: myBackgroundColor,
                          fontWeight: FontWeight.bold),
                    ),
                    onPressed: () => _login(context),
                    shape: StadiumBorder(),
                    color: Colors.green,
                    elevation: 8.0,
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
