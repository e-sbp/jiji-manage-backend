import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:http/http.dart' as http;
import 'package:html/parser.dart' as parser;
import 'package:convert/convert.dart';
import 'package:crypto/crypto.dart' as crypto;
import 'dart:convert';
import 'dart:html';
import 'dart:async';

import 'login.dart';
import 'urls.dart';

/// this is a function to show a toast

showToast(String message) {
  var snackbar = document.querySelector('#snackbar');
  snackbar.text = message;
  snackbar.classes.add("show");
  Future.delayed(Duration(milliseconds: 3000), () {
    snackbar.classes.remove("show");
    snackbar.classes.add("");
  });
}

/// This will print errors to console

void logError(String code, String message) =>
    print('Error: $code\nError Message: $message');

/// Confirm if string is empty

bool isStringEmpty(String str) =>
    str == null || str == '' || str.isEmpty ? true : false;

/// Capitalize any string

String capitalize(String s) => s[0].toUpperCase() + s.substring(1);

/// Let us log out

Future<dynamic> handleSignOut(String token, String username) async {
  final response = await http.get(
    '$proxyUrl$baseUrl/100/user/logout/',
    headers: {
      'token': token,
      'user_name': username,
      'api_key': apiKey,
    },
  );
  if (response.statusCode == 200) {
    Map resp = json.decode(response.body);
    Map res = resp['Response'][0];
    return res;
  } else {
    logError("${response.statusCode}", "${response.body}");
    return "Something went wrong. Please try again later";
  }
}

/// Let us logout, from anywhere :)

void logout(BuildContext context, String token, String username) async {
  if (isStringEmpty(token) || isStringEmpty(username)) return;
  showToast("Please wait...");
  try {
    var element = document.getElementById('map-canvas');
    if (element != null) {
      document.body.children.remove(element);
    }
    await handleSignOut(token, username).then((res) {
      final errorCode = res['errorCode'];
      if (errorCode == null) {
        showToast("Error while logging out");
        return;
      }
      if (int.tryParse(errorCode) == 200) {
        showToast('Successful logout');
        Navigator.of(context)
            .pushReplacement(MaterialPageRoute(builder: (_) => LoginScreen()));
      } else {
        logError("LOGOUT ERROR", "Error: $errorCode");
        showToast("Failed log out");
      }
    });
  } catch (err) {
    showToast("Something went wrong. Please try again later");
    print("$err");
  }
}

/// Let us try to run some html string in dart

htmlRender() {
  var document = parser
      .parse('<body>Hello world! <a href="www.html5rocks.com">HTML5 rocks!');
  print(document.outerHtml);
}

/// Here, we shall generate color code for each device from provided string

String stringToColour(String str) {
  var hash = 0;
  for (var i = 0; i < str.length; i++) {
    hash = str.codeUnitAt(i) + ((hash << 5) - hash);
  }
  String colour = '#';
  for (var i = 0; i < 3; i++) {
    var value = (hash >> (i * 8)) & 0xFF;
    colour += (value.toRadixString(16));
  }
  return colour;
}

class HexColor extends Color {
  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll("#", "");
    if (hexColor.length == 6) {
      hexColor = "FF" + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }

  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));
}

/// Read from file in assets

Future<String> getFileData(String path) async {
  return await rootBundle.loadString(path);
}

/// Generate MD5 hash
generateMd5(String data) {
  var content = Utf8Encoder().convert(data);
  var md5 = crypto.md5;
  var digest = md5.convert(content);
  return hex.encode(digest.bytes);
}

/// Generate user profile picture using Gravatar
/// A service that generates an avatar based on provided string that has been MD5 hashed
String userProfilePicture(String username, String size) {
  return 'https://www.gravatar.com/avatar/${generateMd5(username)}?d=identicon&s=$size';
}

/// Used to parse time received from server

parserReceivedTime(String time) {
  String yr2hr = time.substring(0, 8);
  String min2ms = time.substring(8);
  return DateTime.parse('$yr2hr $min2ms');
}

/// Check that string entered is a valid email address

bool validateEmail(String email) {
  RegExp regExp = RegExp(
    r"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}",
    caseSensitive: false,
    multiLine: false,
  );
  return regExp.hasMatch(email);
}

/// Widget for loading screens

Widget loadingWidget() {
  return Container(
    color: Colors.white,
    alignment: Alignment.center,
    child: Column(
      children: <Widget>[
        CircularProgressIndicator(),
        SizedBox(height: 10.0),
        Text(
          'Loading...',
          style: TextStyle(fontSize: 18.0, color: Colors.black),
          textAlign: TextAlign.center,
        ),
      ],
      mainAxisAlignment: MainAxisAlignment.center,
    ),
  );
}
