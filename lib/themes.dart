/// We shall set themes here
import 'package:flutter/material.dart';

import 'colors.dart';

// build main theme
final ThemeData myAppTheme = buildTheme();

ThemeData buildTheme() {
  final ThemeData base = ThemeData.light();
  return base.copyWith(
    accentColor: myAccentColor,
    primaryColor: myPrimaryColor,
    primaryColorDark: myDarkPrimaryColor,
    dividerColor: myDividerColor,
    primaryColorLight: myLightPrimaryColor,
    scaffoldBackgroundColor: myBackgroundColor,
    buttonColor: Colors.green,
    cardColor: myBackgroundColor,
    errorColor: myErrorColor,
    textSelectionColor: myPrimaryColor,
    textTheme: _buildAppTextTheme(base.textTheme),
    primaryTextTheme: _buildAppTextTheme(base.accentTextTheme),
    accentTextTheme: _buildAppTextTheme(base.accentTextTheme),
    // applied a border around our input fields
    inputDecorationTheme: InputDecorationTheme(
      border: OutlineInputBorder(
        // radius is of elliptical shape
        borderRadius: const BorderRadius.all(Radius.elliptical(20, 20)),
        gapPadding: 10.0,
      ),
    ),
  );
}

// theme for text
TextTheme _buildAppTextTheme(TextTheme base) {
  return base
      .copyWith(
        headline: base.headline.copyWith(
          fontWeight: FontWeight.w900,
        ),
        title: base.title.copyWith(
          fontSize: 18.0,
          fontWeight: FontWeight.w500,
        ),
        caption: base.caption.copyWith(
          fontSize: 14.0,
          fontWeight: FontWeight.w500,
        ),
      )
      .apply(
        fontFamily: 'Lato',
        displayColor: myAccentColor,
        bodyColor: myAccentColor,
      );
}
