import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'register_user.dart';
import 'title.dart';
import 'extras.dart';
import 'urls.dart';

class EditUserScreen extends StatefulWidget {
  final String token;
  final String username;

  EditUserScreen(this.token, this.username);

  _EditUserScreenState createState() => _EditUserScreenState();
}

class _EditUserScreenState extends State<EditUserScreen> {
  List<dynamic> users = [];

  @override
  void initState() {
    super.initState();
    loadUsers();
  }

  void loadUsers() async {
    setState(() => users = []);
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => users = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/users/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() {
            users = json.decode(response.body);
            landingState.setState(() => landingState.refresh = loadUsers);
          });
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => users = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => users = []);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Color(0xFFEEEEEE),
        ),
        SingleChildScrollView(
          controller: landingState.controller,
          child: users.isEmpty
              ? Container(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 30.0),
                        Text(
                          "Waiting for users...",
                          style: TextStyle(color: Colors.black, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                  height: MediaQuery.of(context).size.height / 2,
                )
              : Container(
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width - 100,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(
                        vertical: 18.0,
                        horizontal: 5.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 25.0),
                      child: MediaQuery.of(context).size.width < 920
                          ? user()
                          : ListView(
                              children: [
                                SizedBox(height: 1.0),
                                Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'images/ic_barcode.jpg',
                                      scale: 2.0,
                                    ),
                                    SizedBox(height: 10.0),
                                    Material(
                                      child: Text(
                                        'E-SBP',
                                        style: TextStyle(
                                          fontSize: 24.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                // spacer
                                SizedBox(height: 10.0),
                                Container(
                                  margin: const EdgeInsets.all(30.0),
                                  padding: const EdgeInsets.all(1.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: Colors.black,
                                    ),
                                  ),
                                  child: tableView(),
                                  width: MediaQuery.of(context).size.width,
                                ),
                                // spacer
                                SizedBox(height: 10.0),
                              ],
                            ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  /// Create the table of users, with possible changes
  Widget tableView() {
    List<TableRow> _children = [
      _buildTableHeaders(
        'Profile Picture,ID,First Name,Last Name,Email Address,Phone Number,Edit,Status',
        16.0,
      )
    ];
    for (int i = 0; i < users.length; i++) {
      _children.add(_buildTableDetails(users[i], 14.0));
    }
    return Table(
      defaultColumnWidth: FixedColumnWidth(
        MediaQuery.of(context).size.width > 460 ? 80.0 : 30.0,
      ),
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        verticalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: _children,
    );
  }

  /// Set up the table column titles
  TableRow _buildTableHeaders(String columnNames, num fontSize) {
    return TableRow(
      children: columnNames.split(',').map((name) {
        return Container(
          alignment: Alignment.center,
          child: Text(
            name,
            style: TextStyle(fontSize: fontSize, color: Colors.black),
          ),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }

  /// Set up the rows containing user details
  TableRow _buildTableDetails(Map user, num fontSize) {
    return TableRow(
      children: [
        Container(
          alignment: Alignment.center,
          child: Image.network(
            userProfilePicture(user['user_id'] ?? '', '32'),
            scale: 0.9,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['user_id'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['first_name'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['last_name'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            user['email_address'],
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            '${user['phone_number']}'.replaceFirst('254', '0'),
            style: TextStyle(
              fontSize: fontSize,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: IconButton(
            icon: Icon(
              Icons.edit,
              color: user["active"] ? Colors.black : Colors.grey,
            ),
            onPressed: user["active"] ? () => _editUser(user) : null,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: IconButton(
            icon: Icon(
              !user["active"] ? Icons.restore : Icons.remove_circle,
              color: Colors.black,
            ),
            onPressed: () => _disableUser(
                user['user_id'], user['user_name'], user["active"] ? 0 : 1),
          ),
          padding: EdgeInsets.all(8.0),
        ),
      ],
    );
  }

  void _editUser(Map user) {
    Map<String, String> details = {
      'user_name': user['user_name'],
      "first_name": user["first_name"],
      "last_name": user["last_name"],
      "user_role": null,
      "user_mssdn": '${user["phone_number"]}'.replaceFirst('254', ''),
      "user_id": user["user_id"],
      'email_address': user['email_address'],
    };
    Navigator.of(context).push(MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppTitle(() => Navigator.of(context).pop()),
        body: RegisterUserScreen(widget.token, details: details),
      );
    }));
  }

  void _disableUser(String user_id, String username, int status) async {
    if (isStringEmpty(apiKey) ||
        isStringEmpty(widget.token) ||
        isStringEmpty(user_id)) {
      showToast("Something missing. Please try again later");
    } else if (username == widget.username || username == "micah") {
      showToast("Oops! Mission Impossible");
    } else {
      await http.put(
        '$proxyUrl$baseUrl/40/users/disable/${user_id}/$status',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          Map resp = json.decode(response.body);
          var res = resp['Response'][0];
          if (res is String) {
            showToast(res);
            return;
          }
          final errorCode = res['errorCode'];
          final desc = res['desc'];
          if (desc == null || errorCode == null) {
            showToast("Error while registering in");
            return;
          }
          showToast("$desc");
          print("$errorCode");
          if (int.parse(errorCode) == 200) {
            // Reload users screen
            loadUsers();
          }
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
      });
    }
  }

  Widget user() {
    num textSize = MediaQuery.of(context).size.width > 475 ? 16.0 : 14.0;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: MediaQuery.of(context).size.width < 800 ? 1 : 2,
      ),
      itemCount: users.length,
      itemBuilder: (context, index) {
        return Container(
          child: Card(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(15.0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Container(
                      width: 50.0,
                      height: 50.0,
                      margin: EdgeInsets.only(left: 2.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            userProfilePicture(users[index]['user_id'], '32'),
                            scale: 0.75,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.person, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            users[index]["first_name"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.person_outline, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            users[index]["last_name"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.person_pin_circle, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            users[index]["user_id"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.phone, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            '+${users[index]["phone_number"]}',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.email, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            users[index]["email_address"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Expanded(
                            child: IconButton(
                              icon: Row(
                                children: <Widget>[
                                  Icon(
                                    Icons.edit,
                                    color: users[index]["active"]
                                        ? Colors.blueGrey
                                        : Colors.grey,
                                    size: 14.0,
                                  ),
                                  SizedBox(width: 1.0),
                                  Text(
                                    "Edit",
                                    style: TextStyle(
                                      color: users[index]["active"]
                                          ? Colors.black
                                          : Colors.grey,
                                      fontSize: 14.0,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              onPressed: users[index]["active"]
                                  ? () => _editUser(users[index])
                                  : null,
                            ),
                          ),
                          Expanded(
                            child: IconButton(
                              icon: Row(
                                children: <Widget>[
                                  Icon(
                                    !users[index]["active"]
                                        ? Icons.restore
                                        : Icons.remove_circle,
                                    color: Colors.blueGrey,
                                    size: 14.0,
                                  ),
                                  SizedBox(width: 1.0),
                                  Text(
                                    "${users[index]["active"] ? 'Disable' : 'Enable'}",
                                    style: TextStyle(
                                      color: Colors.black,
                                      fontSize: 14.0,
                                    ),
                                    textAlign: TextAlign.center,
                                  ),
                                ],
                              ),
                              onPressed: () => _disableUser(
                                users[index]["user_id"],
                                users[index]['user_name'],
                                users[index]["active"] ? 0 : 1,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            elevation: 3.0,
          ),
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        );
      },
    );
  }
}
