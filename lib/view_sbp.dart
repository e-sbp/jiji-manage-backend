import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'extras.dart';
import 'colors.dart';
import 'urls.dart';

class ViewSBPScreen extends StatefulWidget {
  final String token;

  ViewSBPScreen(this.token);

  _ViewSBPScreenState createState() => _ViewSBPScreenState();
}

class _ViewSBPScreenState extends State<ViewSBPScreen>
    with TickerProviderStateMixin {
  List<dynamic> sbps = [];
  Map errorCodes = {
    "200": "Proper Businesses",
    "400": "Incorrect Business IDs",
    "403": "Incorrect Business Locations",
    "408": "Invalid QR Codes",
    "201": "Business Permits Already Verified",
    "202": "Newly Registered Business",
    "203": "Exceeded Verification Cycle",
  };
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();
    // Control blinking widget
    _animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 1));
    _animationController.repeat();
    loadSBPs();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  void loadSBPs() async {
    setState(() => sbps = []);
    if (isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => sbps = []);
    } else {
      await http.get(
        '$proxyUrl$sbpUrl/400/qrcode/all',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() {
            sbps = json.decode(response.body);
            landingState.setState(() => landingState.refresh = loadSBPs);
          });
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => sbps = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => sbps = []);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Color(0xFFEEEEEE),
        ),
        SingleChildScrollView(
          controller: landingState.controller,
          child: sbps.isEmpty
              ? Container(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 30.0),
                        Text(
                          "Waiting for businesses...",
                          style: TextStyle(color: Colors.black, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                  height: MediaQuery.of(context).size.height / 2,
                )
              : Container(
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width - 100,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(
                        vertical: 18.0,
                        horizontal: 5.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 25.0),
                      child: Column(
                        children: <Widget>[
                          Expanded(
                            child: MediaQuery.of(context).size.width < 920
                                ? sbp()
                                : ListView(
                                    children: [
                                      SizedBox(height: 1.0),
                                      Column(
                                        children: <Widget>[
                                          Image.asset(
                                            'images/ic_barcode.jpg',
                                            scale: 2.0,
                                          ),
                                          SizedBox(height: 10.0),
                                          Material(
                                            child: Text(
                                              'E-SBP',
                                              style: TextStyle(
                                                fontSize: 24.0,
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                      // spacer
                                      SizedBox(height: 10.0),
                                      Container(
                                        margin: const EdgeInsets.all(30.0),
                                        padding: const EdgeInsets.all(1.0),
                                        decoration: BoxDecoration(
                                          border: Border.all(
                                            width: 1,
                                            color: Colors.black,
                                          ),
                                        ),
                                        child: tableView(),
                                        width:
                                            MediaQuery.of(context).size.width,
                                      ),
                                      // spacer
                                      SizedBox(height: 10.0),
                                    ],
                                  ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  /// Create the table of markets
  Widget tableView() {
    List<TableRow> _children = [
      _buildTableHeaders(
        ',Business ID,Business Name,Plot Number,Email Address,Physical Address,Phone Number,Market ID,Status',
        16.0,
      )
    ];
    for (int i = 0; i < sbps.length; i++) {
      _children.add(_buildTableDetails(sbps[i], 14.0));
    }
    return Table(
      defaultColumnWidth: FixedColumnWidth(
        MediaQuery.of(context).size.width > 460 ? 50.0 : 30.0,
      ),
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        verticalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: _children,
    );
  }

  /// Set up the table column titles
  TableRow _buildTableHeaders(String columnNames, num fontSize) {
    return TableRow(
      children: columnNames.split(',').map((name) {
        return Container(
          alignment: Alignment.center,
          child: Text(
            name,
            style: TextStyle(fontSize: fontSize, color: Colors.black),
          ),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }

  /// Set up the rows containing market details
  TableRow _buildTableDetails(Map sbp, num fontSize) {
    return TableRow(
      children: [
        Container(
          alignment: Alignment.center,
          child: Row(
            children: <Widget>[
              FadeTransition(
                opacity: _animationController,
                child: Container(
                  width: 7.5,
                  height: 7.5,
                  decoration: BoxDecoration(
                    color: colorCodes[sbp["errorcode"]],
                    shape: BoxShape.circle,
                  ),
                  alignment: Alignment.center,
                ),
              ),
              SizedBox(width: 2.0),
              Image.network(
                'https://ui-avatars.com/api/?name=${sbp["business_name"]}' +
                    '&bold=true&size=24&background=0D8ABC&color=fff&rounded=true'
                        .replaceAll(' ', "+"),
                scale: 0.9,
              ),
            ],
            // scrollDirection: Axis.horizontal,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            sbp['business_id'].toString(),
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            sbp['business_name'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            sbp['plot_number'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            sbp['mail_address'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            sbp['physical_address'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            sbp['mssdn'],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        GestureDetector(
          child: Container(
            alignment: Alignment.center,
            child: Text(
              '${sbp['market_id']}',
              style: TextStyle(
                fontSize: fontSize,
                color: Colors.blue,
                decoration: TextDecoration.underline,
                decorationColor: Colors.blue,
              ),
              textAlign: TextAlign.center,
            ),
            padding: EdgeInsets.all(8.0),
          ),
          // TODO: Build and link to page of specific market
          onTap: () => showToast("Go to market"),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            errorCodes[sbp["errorcode"]],
            style: TextStyle(fontSize: fontSize, color: Colors.black),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
      ],
    );
  }

  /// Screen for small devices
  Widget sbp() {
    num textSize = MediaQuery.of(context).size.width > 475 ? 16.0 : 14.0;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: MediaQuery.of(context).size.width < 800 ? 1 : 2,
      ),
      itemCount: sbps.length,
      itemBuilder: (context, index) {
        return Container(
          child: Card(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(15.0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Container(
                      width: 50.0,
                      height: 50.0,
                      margin: EdgeInsets.only(left: 2.0),
                      decoration: BoxDecoration(
                        border: Border.all(
                          color: Colors.black,
                          width: 2.0,
                        ),
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                            'https://ui-avatars.com/api/?name=${sbps[index]["business_name"]}&size=32&background=0D8ABC&color=fff&rounded=true&bold=true'
                                .replaceAll(' ', "+"),
                            scale: 0.75,
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.label, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            sbps[index]["business_id"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.person_outline, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            sbps[index]["business_name"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.place, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            sbps[index]["plot_number"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.phone, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            '+${sbps[index]["mssdn"]}',
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.email, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            sbps[index]["mail_address"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(Icons.home, color: Colors.blueGrey),
                        Expanded(
                          child: Text(
                            sbps[index]["physical_address"],
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    GestureDetector(
                      child: Row(
                        children: <Widget>[
                          Icon(Icons.business_center, color: Colors.blueGrey),
                          Expanded(
                            child: Text(
                              '${sbps[index]["market_id"]}',
                              style: TextStyle(
                                color: Colors.blue,
                                fontSize: textSize,
                                decoration: TextDecoration.underline,
                                decorationColor: Colors.blue,
                              ),
                              textAlign: TextAlign.center,
                            ),
                            flex: 1,
                          ),
                        ],
                        mainAxisAlignment:
                            MediaQuery.of(context).size.width > 730
                                ? MainAxisAlignment.center
                                : MainAxisAlignment.start,
                      ),
                      // TODO: Build and link to page of specific market
                      onTap: () => showToast("Go to market"),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        FadeTransition(
                          opacity: _animationController,
                          child: Container(
                            width: 10.0,
                            height: 10.0,
                            decoration: BoxDecoration(
                              color: colorCodes[sbps[index]["errorcode"]],
                              shape: BoxShape.circle,
                            ),
                          ),
                        ),
                        SizedBox(width: 5.0),
                        Text(
                          errorCodes[sbps[index]["errorcode"]],
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: textSize,
                          ),
                          textAlign: TextAlign.start,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            elevation: 3.0,
          ),
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        );
      },
    );
  }
}
