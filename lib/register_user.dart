import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'accent_override.dart';
import 'colors.dart';
import 'extras.dart';
import 'urls.dart';

class RegisterUserScreen extends StatefulWidget {
  final String token;
  final Map<String, String> details;

  RegisterUserScreen(this.token, {this.details});

  _RegisterUserScreenState createState() => _RegisterUserScreenState();
}

class _RegisterUserScreenState extends State<RegisterUserScreen> {
  String username = '', firstname = '', lastname = '', password = '';
  String phone = '', emailAddress = '', role;
  Map<String, String> details = {};
  List<String> roles = ["Administrator", "Clerk"];

  final userNameController = TextEditingController();
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final pinController = TextEditingController();
  final phoneController = TextEditingController();
  final emailController = TextEditingController();

  @override
  void initState() {
    super.initState();
    details = widget.details ?? {};
    username = details['user_name'] ?? '';
    firstname = details['first_name'] ?? '';
    lastname = details['last_name'] ?? '';
    role = details['user_role'];
    phone = details['user_mssdn'] ?? '';
    emailAddress = details['email_address'] ?? '';

    userNameController.text = username;
    firstNameController.text = firstname;
    lastNameController.text = lastname;
    phoneController.text = phone;
    pinController.text = password;
    emailController.text = emailAddress;
  }

  void _username(String str) {
    if (!mounted) return;
    setState(() => username = str.toLowerCase().trim());
  }

  void _firstname(String str) {
    if (!mounted) return;
    setState(() => firstname = str.trim());
  }

  void _lastname(String str) {
    if (!mounted) return;
    setState(() => lastname = str.trim());
  }

  void _email(String str) {
    if (!mounted) return;
    setState(() => emailAddress = str.toLowerCase().trim());
  }

  void _phone(String str) {
    if (!mounted) return;
    setState(() => phone = str.toLowerCase().trim());
  }

  void _role(String str) {
    if (!mounted) return;
    setState(() => role = capitalize(str.trim()));
  }

  void _password(String str) {
    if (!mounted) return;
    setState(() => password = str);
  }

  Future<dynamic> handleRegister(Map<String, String> value) async {
    if (isStringEmpty(widget.token) ||
        isStringEmpty(apiKey) ||
        (details.isNotEmpty & isStringEmpty(details["user_id"]))) {
      return "Something missing. Please try again later";
    }
    var response;
    if (details.isEmpty) {
      response = await http.post(
        '$proxyUrl$baseUrl/500/users/registration/',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    } else {
      response = await http.put(
        '$proxyUrl$baseUrl/30/users/edit/${details["user_id"]}/',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    }
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _registerUser() {
    if (!mounted) return;
    setState(() {
      if (isStringEmpty(firstname)) {
        showToast("Please enter the first name");
      } else if (isStringEmpty(lastname)) {
        showToast("Please enter the last name");
      } else if (isStringEmpty(username)) {
        showToast("Please enter the user name");
      } else if (isStringEmpty(emailAddress)) {
        showToast("Please enter the email address");
      } else if (!validateEmail(emailAddress)) {
        showToast("Please enter a valid email address");
      } else if (isStringEmpty(phone)) {
        showToast("Please enter the phone number");
      } else if (isStringEmpty(password)) {
        showToast("Please enter the password");
      } else if (isStringEmpty(role)) {
        showToast("Please select a role");
      } else {
        showToast("Please wait...");
        handleRegister({
          "user_name": username,
          "first_name": firstname,
          "last_name": lastname,
          "user_role": role,
          "user_mssdn": "254${int.tryParse(phone)}",
          "user_pin": password,
          "email_address": emailAddress,
        }).then((res) {
          if (res is String) {
            showToast(res);
            return;
          }
          final errorCode = res['errorCode'];
          final desc = res['desc'];
          if (desc == null || errorCode == null) {
            showToast("Error while registering in");
            return;
          }
          showToast("$desc");
          print("$errorCode");
          if (int.parse(errorCode) == 200) {
            if (details.isEmpty) {
              landingState.setState(() => landingState.screen = "home");
            } else {
              Navigator.of(context).pop();
            }
          }
        }).catchError((err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Color(0xFFEEEEEE),
        ),
        SingleChildScrollView(
          controller: landingState.controller,
          child: Container(
            child: Center(
              child: Container(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 18.0, horizontal: 15.0),
                child: Column(
                  children: <Widget>[
                    // Given a child, this widget forces child to have a specific width and/or height
                    // else it will adjust itself to given dimension
                    SizedBox(height: 1.0),
                    // display children in vertical array
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'images/ic_barcode.jpg',
                          scale: 2.0,
                        ),
                        SizedBox(height: 10.0),
                        Material(
                          child: Text(
                            'E-SBP',
                            style: TextStyle(
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    // spacer
                    SizedBox(height: 10.0),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 + 80,
                      child: Row(
                        children: <Widget>[
                          // First name
                          Flexible(
                            child: AccentOverride(
                              child: TextField(
                                controller: firstNameController,
                                decoration: InputDecoration(
                                  labelText: 'First Name',
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                ),
                                onChanged: _firstname,
                              ),
                            ),
                          ),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10.0)),
                          // Last name
                          Flexible(
                            child: AccentOverride(
                              child: TextField(
                                controller: lastNameController,
                                decoration: InputDecoration(
                                  labelText: 'Last Name',
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                ),
                                onChanged: _lastname,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // spacer
                    SizedBox(height: 10.0),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 + 80,
                      child: Row(
                        children: <Widget>[
                          // Username
                          Expanded(
                            child: AccentOverride(
                              child: TextField(
                                enabled: details.isEmpty ? true : false,
                                decoration: InputDecoration(
                                  labelText: 'Username',
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                ),
                                onChanged: _username,
                                controller: userNameController,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // spacer
                    SizedBox(height: 10.0),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 + 80,
                      child: Row(
                        children: <Widget>[
                          // Username
                          Expanded(
                            child: AccentOverride(
                              child: TextField(
                                enabled: details.isEmpty ? true : false,
                                decoration: InputDecoration(
                                  labelText: 'Email Address',
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                ),
                                onChanged: _email,
                                controller: emailController,
                                keyboardType: TextInputType.emailAddress,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // spacer
                    SizedBox(height: 10.0),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 + 80,
                      child: Row(
                        children: <Widget>[
                          MediaQuery.of(context).size.width >= 250
                              ? Expanded(
                                  flex: 1,
                                  child: Row(
                                    children: <Widget>[
                                      Image.asset('images/ke_flag.png',
                                          scale: 1.2),
                                      MediaQuery.of(context).size.width >= 780
                                          ? Text(' (+254)',
                                              style: TextStyle(
                                                  color: Colors.black))
                                          : SizedBox(width: 0.0, height: 0.0),
                                    ],
                                  ),
                                )
                              : Container(),
                          Padding(
                              padding: EdgeInsets.symmetric(horizontal: 2.0)),
                          // Phone number
                          Expanded(
                            flex: 4,
                            child: AccentOverride(
                              child: TextField(
                                controller: phoneController,
                                decoration: InputDecoration(
                                  labelText: 'Phone number',
                                  helperText: "Don't start with 0",
                                  helperStyle: TextStyle(
                                    color: Colors.black,
                                    fontSize: 12.0,
                                  ),
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                ),
                                keyboardType: TextInputType.phone,
                                onChanged: _phone,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    // spacer
                    SizedBox(height: 10.0),
                    Container(
                      width: MediaQuery.of(context).size.width / 2 + 80,
                      child: Row(
                        children: <Widget>[
                          // Password
                          Flexible(
                            child: AccentOverride(
                              child: TextField(
                                controller: pinController,
                                decoration: InputDecoration(
                                  labelText: 'Password',
                                  border: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.black),
                                  ),
                                ),
                                obscureText: true,
                                onChanged: _password,
                              ),
                              color: myAccentColor,
                            ),
                          ),
                          MediaQuery.of(context).size.width >= 978
                              ? Padding(
                                  padding:
                                      EdgeInsets.symmetric(horizontal: 10.0))
                              : Container(),
                          MediaQuery.of(context).size.width >= 978
                              ? Flexible(
                                  child: roleDropDown(),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                    MediaQuery.of(context).size.width < 978
                        ? Container(
                            child: Column(
                              children: [
                                // spacer
                                SizedBox(height: 10.0),
                                roleDropDown(),
                              ],
                            ),
                            width: MediaQuery.of(context).size.width / 2 + 80,
                          )
                        : Container(),
                    // spacer
                    SizedBox(height: 10.0),
                    // Places the buttons horizontally according to the padding
                    Container(
                      child: Row(
                        children: <Widget>[
                          RaisedButton(
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                color: myBackgroundColor,
                                fontSize: 15.0,
                              ),
                            ),
                            onPressed: () => _registerUser(),
                            shape: StadiumBorder(),
                            color: Colors.green,
                            elevation: 8.0,
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                  ],
                ),
                width: MediaQuery.of(context).size.width,
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget roleDropDown() => Container(
        padding: MediaQuery.of(context).size.width < 978
            ? EdgeInsets.symmetric(vertical: 10.0)
            : EdgeInsets.symmetric(horizontal: 5.0),
        child: Container(
          child: DropdownButton<String>(
            isExpanded: true,
            hint: Text("Please select a role"),
            items: roles
                .map(
                  (item) => DropdownMenuItem(
                    value: item,
                    child: Text(
                      item,
                      style: TextStyle(color: Colors.black),
                    ),
                  ),
                )
                .toList(),
            onChanged: (value) => _role(value),
            value: role,
          ),
        ),
      );
}
