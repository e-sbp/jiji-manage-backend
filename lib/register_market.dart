import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'accent_override.dart';
import 'landing.dart' show landingState;
import 'colors.dart';
import 'extras.dart';
import 'urls.dart';

class RegisterMarketScreen extends StatefulWidget {
  final String token;
  final Map<String, String> details;

  RegisterMarketScreen(this.token, {this.details});

  _RegisterMarketScreenState createState() => _RegisterMarketScreenState();
}

class _RegisterMarketScreenState extends State<RegisterMarketScreen> {
  String marketName = '', marketID = '', userID;
  List<dynamic> users = [];
  List<dynamic> markets = [];
  Map<String, String> details = {};

  final nameController = TextEditingController();

  @override
  void initState() {
    super.initState();

    details = widget.details ?? {};
    marketID = details['market_id'] ?? '';
    marketName = details['market_name'] ?? '';
    userID = details['user_id'];

    nameController.text = marketName;

    loadUsers();
    loadMarkets();
  }

  void _marketName(String str) {
    if (!mounted) return;
    setState(() => marketName = capitalize(str.trim()));
  }

  void _role(String str) {
    if (!mounted) return;
    setState(() => userID = str.toLowerCase().trim());
  }

  void loadUsers() async {
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => users = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/active/users/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() => users = json.decode(response.body));
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => users = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => users = []);
      });
    }
  }

  void loadMarkets() async {
    setState(() => markets = []);
    if (isStringEmpty(apiKey) || isStringEmpty(widget.token)) {
      showToast("Something missing. Try logging in again");
      setState(() => markets = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/markets/',
        headers: {
          'token': widget.token,
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() {
            List resp = json.decode(response.body);
            for (int i = 0; i < resp.length; i++) {
              if (resp[i] is Map) {
                markets.add('${resp[i]['market_name']}'.toLowerCase());
              } else {
                markets.add('${resp[i]}');
              }
            }
          });
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => markets = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => markets = []);
      });
    }
  }

  Future<dynamic> handleRegister(Map<String, String> value) async {
    if (isStringEmpty(widget.token) ||
        isStringEmpty(apiKey) ||
        (details.isNotEmpty & isStringEmpty(details["market_id"]))) {
      return "Something missing. Please try again later";
    }
    print(value);
    var response;
    if (details.isEmpty) {
      response = await http.post(
        '$proxyUrl$baseUrl/500/market/registration/',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    } else {
      response = await http.put(
        '$proxyUrl$baseUrl/30/market/edit/${details["market_id"]}/',
        headers: {
          'token': widget.token,
          'Content-Type': 'application/json',
        },
        body: json.encode(value),
      );
    }
    if (response.statusCode == 200) {
      Map resp = json.decode(response.body);
      Map res = resp['Response'][0];
      return res;
    } else {
      logError("${response.statusCode}", "${response.body}");
      return "Something went wrong. Please try again later";
    }
  }

  void _registerMarket() {
    if (!mounted) return;
    setState(() {
      if (isStringEmpty(marketName)) {
        showToast("Please enter the market name");
      } else if (isStringEmpty(userID)) {
        showToast("Please select the user");
      } else if (markets.contains(marketName.toLowerCase()) &&
          details.isEmpty) {
        showToast("Market already registered");
      } else {
        Map<String, String> values = {
          "market_name": marketName,
          "user_id": userID,
        };
        if (details.isNotEmpty) values['market_id'] = marketID;
        handleRegister(values).then((res) {
          if (res is String) {
            showToast(res);
            return;
          }
          final errorCode = res['errorCode'];
          final desc = res['desc'];
          if (desc == null || errorCode == null) {
            showToast("Error while registering in");
            return;
          }
          showToast("$desc");
          print("$errorCode");
          if (int.parse(errorCode) == 200) {
            if (details.isEmpty) {
              landingState.setState(() => landingState.screen = "home");
            } else {
              Navigator.of(context).pop();
            }
          }
        }).catchError((err) {
          showToast("Something went wrong. Please try again later");
          print("$err");
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      controller: landingState.controller,
      child: users.isEmpty
          ? Container(
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    CircularProgressIndicator(),
                    SizedBox(height: 30.0),
                    Text(
                      "Waiting for users...",
                      style: TextStyle(color: Colors.black, fontSize: 16.0),
                    ),
                  ],
                ),
              ),
              height: MediaQuery.of(context).size.height / 2,
            )
          : Center(
              child: Container(
                color: Colors.white,
                margin: EdgeInsets.symmetric(vertical: 8.0),
                padding: EdgeInsets.all(20.0),
                child: Column(
                  children: <Widget>[
                    // Given a child, this widget forces child to have a specific width and/or height
                    // else it will adjust itself to given dimension
                    SizedBox(height: 20.0),
                    // display children in vertical array
                    Column(
                      children: <Widget>[
                        Image.asset(
                          'images/ic_barcode.jpg',
                          scale: 2.0,
                        ),
                        SizedBox(height: 10.0),
                        Material(
                          child: Text(
                            'E-SBP',
                            style: TextStyle(
                              fontSize: 24.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ],
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    // Market name
                    Container(
                      child: Center(
                        child: AccentOverride(
                          child: TextField(
                            controller: nameController,
                            decoration: InputDecoration(
                              labelText: 'Market Name',
                              border: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.black),
                              ),
                              helperText: "e.g. Agriculture",
                              helperStyle: TextStyle(
                                  color: Colors.black, fontSize: 12.0),
                            ),
                            onChanged: _marketName,
                          ),
                        ),
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    // User assigned to market
                    Container(
                      child: Row(
                        children: [
                          Icon(Icons.person),
                          Flexible(
                            child: DropdownButton(
                              isExpanded: true,
                              hint: Text("Please select a user"),
                              items: users.map((value) {
                                return value is Map
                                    ? DropdownMenuItem<String>(
                                        value: value['user_id'],
                                        child: Text(
                                          '${value['first_name']} ${value['last_name']}',
                                        ),
                                      )
                                    : DropdownMenuItem<String>(
                                        value: value,
                                        child: Text('$value'),
                                      );
                              }).toList(),
                              onChanged: (value) => _role(value),
                              value: userID,
                            ),
                          ),
                        ],
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                    // spacer
                    SizedBox(height: 30.0),
                    // Places the buttons horizontally according to the padding
                    Container(
                      child: Row(
                        children: <Widget>[
                          RaisedButton(
                            child: Text(
                              'Submit',
                              style: TextStyle(
                                color: myBackgroundColor,
                                fontSize: 15.0,
                              ),
                            ),
                            onPressed: () => _registerMarket(),
                            shape: StadiumBorder(),
                            color: Colors.green,
                            elevation: 8.0,
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.end,
                      ),
                      width: MediaQuery.of(context).size.width / 2 + 80,
                    ),
                  ],
                ),
              ),
            ),
    );
  }
}
