import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'landing.dart' show landingState;
import 'extras.dart';
import 'urls.dart';

class ViewDeviceScreen extends StatefulWidget {
  _ViewDeviceScreenState createState() => _ViewDeviceScreenState();
}

class _ViewDeviceScreenState extends State<ViewDeviceScreen> {
  List<dynamic> devices = [];

  @override
  void initState() {
    super.initState();
    loadDevices();
  }

  void loadDevices() async {
    setState(() => devices = []);
    if (isStringEmpty(apiKey)) {
      showToast("Something missing. Try logging in again");
      setState(() => devices = []);
    } else {
      await http.get(
        '$proxyUrl$baseUrl/700/devices/',
        headers: {
          'api_key': apiKey,
          'Content-Type': 'application/json',
        },
      ).then((response) {
        if (response.statusCode == 200) {
          setState(() {
            devices = json.decode(response.body);
            landingState.setState(() => landingState.refresh = loadDevices);
          });
        } else {
          logError("${response.statusCode}", "${response.body}");
          showToast("Something went wrong. Please try again later");
          setState(() => devices = []);
        }
      }).catchError((err) {
        showToast("Something went wrong. Please try again later");
        print("$err");
        setState(() => devices = []);
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          color: Color(0xFFEEEEEE),
        ),
        SingleChildScrollView(
          controller: landingState.controller,
          child: devices.isEmpty
              ? Container(
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        CircularProgressIndicator(),
                        SizedBox(height: 30.0),
                        Text(
                          "Waiting for devices...",
                          style: TextStyle(color: Colors.black, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                  height: MediaQuery.of(context).size.height / 2,
                )
              : Container(
                  child: Center(
                    child: Container(
                      width: MediaQuery.of(context).size.width - 200,
                      height: MediaQuery.of(context).size.height,
                      color: Colors.white,
                      margin: EdgeInsets.symmetric(
                        vertical: 18.0,
                        horizontal: 5.0,
                      ),
                      padding: EdgeInsets.symmetric(vertical: 25.0),
                      child: MediaQuery.of(context).size.width < 920
                          ? device()
                          : ListView(
                              children: [
                                SizedBox(height: 1.0),
                                Column(
                                  children: <Widget>[
                                    Image.asset(
                                      'images/ic_barcode.jpg',
                                      scale: 2.0,
                                    ),
                                    SizedBox(height: 10.0),
                                    Material(
                                      child: Text(
                                        'E-SBP',
                                        style: TextStyle(
                                          fontSize: 24.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                                // spacer
                                SizedBox(height: 10.0),
                                Container(
                                  margin: const EdgeInsets.all(30.0),
                                  padding: const EdgeInsets.all(1.0),
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                      width: 1,
                                      color: Colors.black,
                                    ),
                                  ),
                                  child: tableView(),
                                  width: MediaQuery.of(context).size.width,
                                ),
                                // spacer
                                SizedBox(height: 10.0),
                              ],
                            ),
                    ),
                  ),
                ),
        ),
      ],
    );
  }

  /// Create the table of devices
  Widget tableView() {
    List<TableRow> _children = [
      _buildTableHeaders(',Device ID,Device Name,User ID', 18.0)
    ];
    for (int i = 0; i < devices.length; i++) {
      _children.add(_buildTableDetails(devices[i], 16.0));
    }
    return Table(
      defaultColumnWidth: FixedColumnWidth(
        MediaQuery.of(context).size.width > 460 ? 50.0 : 30.0,
      ),
      border: TableBorder(
        horizontalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
        verticalInside: BorderSide(
          color: Colors.black,
          style: BorderStyle.solid,
          width: 1.0,
        ),
      ),
      children: _children,
    );
  }

  /// Set up the table column titles
  TableRow _buildTableHeaders(String columnNames, num fontSize) {
    return TableRow(
      children: columnNames.split(',').map((name) {
        return Container(
          alignment: Alignment.center,
          child: Text(
            name,
            style: TextStyle(fontSize: fontSize, color: Colors.black),
          ),
          padding: EdgeInsets.all(8.0),
        );
      }).toList(),
    );
  }

  /// Set up the rows containing device details
  TableRow _buildTableDetails(Map device, num fontSize) {
    return TableRow(
      children: [
        Container(
          alignment: Alignment.center,
          child: Icon(
            device["device_name"] == "Android"
                ? Icons.phone_android
                : Icons.phone_iphone,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            device['device_id'].toString(),
            style: TextStyle(
              fontSize: fontSize,
              color: device["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            device['device_name'],
            style: TextStyle(
              fontSize: fontSize,
              color: device["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
        Container(
          alignment: Alignment.center,
          child: Text(
            device['user_id'],
            style: TextStyle(
              fontSize: fontSize,
              color: device["active"] ? Colors.black : Colors.grey,
            ),
            textAlign: TextAlign.center,
          ),
          padding: EdgeInsets.all(8.0),
        ),
      ],
    );
  }

  /// Screen for small devices
  Widget device() {
    num textSize = MediaQuery.of(context).size.width > 475 ? 16.0 : 14.0;
    return GridView.builder(
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: MediaQuery.of(context).size.width < 800 ? 1 : 2,
      ),
      itemCount: devices.length,
      itemBuilder: (context, index) {
        return Container(
          child: Card(
            color: Colors.white,
            child: Container(
              padding: EdgeInsets.all(15.0),
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 20.0),
                    Container(
                      alignment: Alignment.center,
                      child: Icon(
                        devices[index]["device_name"] == "Android"
                            ? Icons.phone_android
                            : Icons.phone_iphone,
                        color: devices[index]["active"]
                            ? Colors.black
                            : Colors.grey,
                      ),
                      padding: EdgeInsets.all(8.0),
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.smartphone,
                          color: devices[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            devices[index]['device_name'],
                            style: TextStyle(
                              fontSize: textSize,
                              color: devices[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.pin_drop,
                          color: devices[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            '${devices[index]['device_id']}',
                            style: TextStyle(
                              fontSize: textSize,
                              color: devices[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                    SizedBox(height: 20.0),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.person,
                          color: devices[index]["active"]
                              ? Colors.blueGrey
                              : Colors.grey,
                        ),
                        Expanded(
                          child: Text(
                            devices[index]["user_id"],
                            style: TextStyle(
                              color: devices[index]["active"]
                                  ? Colors.black
                                  : Colors.grey,
                              fontSize: textSize,
                            ),
                            textAlign: TextAlign.center,
                          ),
                          flex: 1,
                        ),
                      ],
                      mainAxisAlignment: MediaQuery.of(context).size.width > 730
                          ? MainAxisAlignment.center
                          : MainAxisAlignment.start,
                    ),
                  ],
                ),
              ),
            ),
            elevation: 3.0,
          ),
          margin: EdgeInsets.symmetric(horizontal: 10.0, vertical: 5.0),
        );
      },
    );
  }
}
