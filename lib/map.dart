import 'package:flutter/material.dart';
import 'package:google_maps/google_maps.dart' as gmap;
import 'dart:html';

import 'title.dart';
import 'urls.dart';
import 'extras.dart';

/// On this screen, we render the dynamic map section of app
/// Courtesy of https://github.com/a14n/dart-google-maps
/// This describes the library to use Google Maps JavaScript API v3 from Dart scripts.

class MapScreen extends StatefulWidget {
  final Map<String, List<Map<String, dynamic>>> all_locations;
  final num zoom;

  MapScreen(this.all_locations, {this.zoom = 15});

  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen> {
  AppBar _appBar;
  DivElement _map = DivElement();
  var deviceCoordinates = <gmap.LatLng>[];

  /// Data for the markers consisting of a name, a LatLng and a zIndex for
  /// the order in which these markers should display on top of each
  /// other.
  var positions = [];

  /// Add script tag for rendering google maps
  var mapScript = ScriptElement()
    ..id = "maps-tag"
    ..type = 'application/javascript'
    ..src = 'https://maps.googleapis.com/maps/api/js?key=$googleMapsApiKey';

  @override
  void initState() {
    super.initState();
    document.body.children.add(mapScript);
    _appBar = AppTitle(() => Navigator.of(context).pop());
  }

  @override
  void dispose() {
    super.dispose();
    var element = document.getElementById('map-canvas');
    if (element != null) {
      document.body.children.remove(element);
    }
    try {
      document.body.children.remove(mapScript);
    } catch (err) {
      print(err);
    }
    deviceCoordinates = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _appBar,
      body: Container(
        child: Center(
          child: RaisedButton(
            onPressed: _loadMap,
            color: Colors.blue,
            shape: StadiumBorder(),
            child: Row(
              children: <Widget>[
                Icon(Icons.map, color: Colors.white),
                SizedBox(width: 5.0),
                Text("Show Map", style: TextStyle(color: Colors.white)),
              ],
              mainAxisSize: MainAxisSize.min,
            ),
          ),
        ),
      ),
    );
  }

  void _loadMap() {
    _map.id = "map-canvas";
    document.body.children.add(_map);

    final mapCanvas = document.getElementById("map-canvas");
    if (mapCanvas != null) {
      mapCanvas.style.marginTop = "${_appBar.preferredSize.height ?? '56'}";
      final mapOptions = gmap.MapOptions()
        ..zoom = widget.zoom
        ..center = gmap.LatLng(center['latitude'], center['longitude'])
        ..fullscreenControl = false
        ..streetViewControlOptions = (gmap.StreetViewControlOptions()
          ..position = gmap.ControlPosition.LEFT_TOP)
        ..zoomControlOptions = (gmap.ZoomControlOptions()
          ..position = gmap.ControlPosition.LEFT_BOTTOM
          ..style = gmap.ZoomControlStyle.SMALL);
      final map = gmap.GMap(mapCanvas, mapOptions);

      for (String deviceID in widget.all_locations.keys) {
        List location = widget.all_locations[deviceID];
        drawPolyLine(location, map);

        // get first and last coordinate of each user/SBP during the given duration
        var first = location.first;
        var last = location.last;

        // let 1st position marker be less significant, hence negative value
        positions.add([
          "Initial\n${first["name"]}",
          first["latitude"],
          first["longitude"],
          -1,
        ]);
        positions.add([
          "Latest\n${last["name"]}",
          last["latitude"],
          last["longitude"],
          4,
        ]);
      }

      setMarkers(map, positions);
    }
  }

  /// Function to draw polyline on map
  drawPolyLine(List locations, gmap.GMap map) {
    // For each result, get latitude and longitude then add to list
    for (Map location in locations) {
      deviceCoordinates.add(
        gmap.LatLng(
          location["latitude"],
          location["longitude"],
        ),
      );
    }
    // here draw the line
    gmap.Polyline(
      gmap.PolylineOptions()
        ..path = deviceCoordinates
        ..geodesic = true
        ..strokeColor = '${stringToColour('99885775987')}'
        ..strokeOpacity = 1.0
        ..strokeWeight = 2,
    ).map = map;
    deviceCoordinates = [];
  }

  /// Add markers to the map
  void setMarkers(gmap.GMap map, List locations) async {
    // Marker sizes are expressed as a Size of X,Y
    // where the origin of the image (0,0) is located
    // in the top left of the image.

    // Origins, anchor positions and coordinates of the marker
    // increase in the X direction to the right and in
    // the Y direction down.
    final image = gmap.Icon()
      ..url = "assets/images/streetview.png"
      // The origin for this image is 0,0.
      ..origin = gmap.Point(0, 0)
      // // The anchor for this image is the foot of person at 0,30.
      ..anchor = gmap.Point(0, 30);

    // Shapes define the clickable region of the icon.
    // The type defines an HTML &lt;area&gt; element 'poly' which
    // traces out a polygon as a series of X,Y points. The final
    // coordinate closes the poly by connecting to the first
    // coordinate.
    final shape = gmap.MarkerShape()
      ..coords = [1, 1, 1, 20, 18, 20, 18, 1]
      ..type = 'poly';

    // Set up the center for the map
    gmap.Marker(gmap.MarkerOptions()
      ..position = gmap.LatLng(center['latitude'], center['longitude'])
      ..map = map
      ..title = "County Revenue Office");

    // Set up the points for the various coordinates received
    for (final location in locations) {
      final myLatLng = gmap.LatLng(location[1] as num, location[2] as num);
      final marker = gmap.Marker(gmap.MarkerOptions()
        ..position = myLatLng
        ..map = map
        ..icon = image
        ..shape = shape
        ..title = location[0] as String
        ..zIndex = location[3] as int);
      const contentString = '<div id="content">'
          '<div id="siteNotice">'
          '</div>'
          '<h1 id="firstHeading" class="firstHeading">Uluru</h1>'
          '<div id="bodyContent">'
          '<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large '
          'sandstone rock formation in the southern part of the '
          'Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) '
          'south west of the nearest large town, Alice Springs; 450&#160;km '
          '(280&#160;mi) by road. Kata Tjuta and Uluru are the two major '
          'features of the Uluru - Kata Tjuta National Park. Uluru is '
          'sacred to the Pitjantjatjara and Yankunytjatjara, the '
          'Aboriginal people of the area. It has many springs, waterholes, '
          'rock caves and ancient paintings. Uluru is listed as a World '
          'Heritage Site.</p>'
          '<p>Attribution: Uluru, <a href="http://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">'
          'http://en.wikipedia.org/w/index.php?title=Uluru</a> '
          '(last visited June 22, 2009).</p>'
          '</div>'
          '</div>';

      final infowindow = gmap.InfoWindow(gmap.InfoWindowOptions()
        ..content = contentString
        ..maxWidth = 200);

      marker.onClick.listen((e) {
        infowindow.open(map, marker);
      });
    }
  }
}
